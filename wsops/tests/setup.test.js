if (!process.env.NODE_ENV) process.env.NODE_ENV = 'test';

require('mocha-steps');

require('./setup/chai.setup');
require('./setup/knex.setup');
require('./setup/axios.setup');
require('./setup/webservice.setup');

// require('./setup/checks.setup');
