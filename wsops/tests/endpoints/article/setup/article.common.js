const Action = require('../../../utils/Action');

const User = require('../../../../engine/datadealer/User');
const UserDetails = require('../../../../engine/datadealer/UserDetails');
const UserRole = require('../../../../engine/datadealer/UserRole');
const Article = require('../../../../engine/datadealer/Article');
const Comment = require('../../../../engine/datadealer/Comment');
const Category = require('../../../../engine/datadealer/Category');
const CategoryHasArticle = require('../../../../engine/datadealer/CategoryHasArticle');

const {
  articleData,
  commentData,
  // articleHasCommentData,
  userData,
  userDetailsData,
  userRoleData,
  categoryData,
  categoryHasArticleData,
} = require('./json/article.common');

module.exports = class ArticleSetup extends Action {
  action = {
    insertUser () {
      const context = this.context();
      const dataSet = userData(context);
      const query = User.query()
        .insertAndFetch(dataSet);

      return query
        .then((user) => {
          // console.log(user);
          context.uid = user.uid;
          context.email = user.email;
        });
    },
    insertUserDetails () {
      const context = this.context();
      const dataSet = userDetailsData(context);
      const query = UserDetails.query()
        .insertAndFetch(dataSet);

      return query
        .then((details) => {
          // console.log(details);
          context.pseudo = details.pseudo;
        });
    },
    insertUserRole () {
      const context = this.context();
      const dataSet = userRoleData(context);
      const query = UserRole.query()
        .insertAndFetch(dataSet);

      return query
        .then((userRole) => {
          context.role = userRole.role;
        });
    },
    insertCategory () {
      const context = this.context();
      const dataSet = categoryData(context);
      const query = Category.query()
        .insertAndFetch(dataSet);

      return query
        .then((category) => {
          context.catid = category.cid;
        });
    },
    insertArticle () {
      const context = this.context();
      const dataSet = articleData(context);
      const query = Article.query()
        .insertAndFetch(dataSet);

      return query
        .then((article) => {
          // console.log(article);
          context.aids.push(article.aid);
        });
    },
    insertComment () {
      const context = this.context();
      const dataSet = commentData(context);
      const query = Comment.query()
        .insertAndFetch(dataSet);

      return query
        .then((comment) => {
          context.cid = comment.cid;
        });
    },
    insertCategoryHasArticle () {
      const context = this.context();
      const dataSet = categoryHasArticleData(context);
      // console.log(dataSet);
      const query = CategoryHasArticle.query()
        .insertAndFetch(dataSet);

      return query;
    },
  }
};
