const bcrypt = require('bcrypt');

const userData = () => ({
  email: 'test@test.fr',
  password: bcrypt.hashSync('test', 10),
});

const userDetailsData = ({ uid }) => ({
  userUid: uid,
  lname: 'Prenom',
  fname: 'Nom',
  pseudo: 'Pseudo',
});

const userRoleData = ({ uid }) => ({
  userUid: uid,
  role: 'admin',
});

const categoryData = () => ({
  title: 'CategoryTitle',
});

const articleData = ({ uid }) => ({
  user_uid: uid,
  title: 'testingTitle',
  body: 'testingBody',
  created_at: '01/12/2021',
});

const commentData = ({ aids, uid }) => ({
  article_aid: aids,
  user_uid: uid,
  title: 'testingTitle',
  body: 'testingBody',
  created_at: '01/12/2021',
});

const categoryHasArticleData = ({ aids, catid }) => ({
  article_aid: aids,
  category_cid: catid,
});

module.exports = {
  articleData,
  commentData,
  userData,
  userDetailsData,
  userRoleData,
  categoryData,
  categoryHasArticleData,
};
