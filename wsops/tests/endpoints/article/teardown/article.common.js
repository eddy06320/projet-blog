const Action = require('../../../utils/Action');
const database = require('../../../../engine/knex');

module.exports = class ArticleTeardown extends Action {
  action = {
    deleteArticle () {
      const { aids } = this.context();

      return database('article')
        .delete()
        .whereIn('aid', aids);
    },
    deleteComment () {
      const { cid } = this.context();

      // si on n'a pas de cid (comme dans un post) on sort
      if (!cid) return undefined;

      return database('comment')
        .delete()
        .where({ cid });
    },
    deleteUser () {
      const { uid } = this.context();

      return database('user')
        .delete()
        .where({ uid });
    },
    deleteUserRole () {
      const { uid } = this.context();
      if (!uid) return undefined;

      return database('userRole')
        .delete()
        .where({ userUid: uid });
    },
    deleteUserDetails () {
      const { uid } = this.context();
      if (!uid) return undefined;

      return database('userDetails')
        .delete()
        .where({ userUid: uid });
    },
    deleteCategory () {
      const { catid } = this.context();

      if (!catid) return undefined;

      return database('category')
        .delete()
        .where('cid', catid);
    },
    deleteCategoryHasArticle () {
      const { catid } = this.context();

      if (!catid) return undefined;

      return database('category_has_article')
        .delete()
        .where({ category_cid: catid });
    },
  }
};
