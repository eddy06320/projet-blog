const { expect } = require('chai');

const ArticleModel = require('../../../../engine/datadealer/Article');
const CommentModel = require('../../../../engine/datadealer/Comment');
const CategoryModel = require('../../../../engine/datadealer/Category');
const UserDetailsModel = require('../../../../engine/datadealer/UserDetails');
const UserRoleModel = require('../../../../engine/datadealer/UserRole');

const ArticleModelKeys = ArticleModel.tableColumns;
const CommentModelKeys = CommentModel.tableColumns;
const CategoryModelKeys = CategoryModel.tableColums;
const UserDetailsModelKeys = UserDetailsModel.tableColumns;
const UserRoleModelKeys = UserRoleModel.tableColumns;

describe('[ROUTES] article', () => {
  describe('#GET', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('should return an article without httpQuery: {}', () => {
      const payload = require('../json/article.get')(testContext);

      return wsops
        .get(`/articles/${payload.aid[0]}`)
        .then((response) => {
          console.log(response.body);
          expect(response.error.text).to.be.equal(undefined);
          expect(response).to.have.status(200);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(ArticleModelKeys);
          expect(response.body).to.not.have.own.property('hasUser');
          expect(response.body).to.not.have.own.property('hasComment');
          expect(response.body).to.not.have.own.property('hasCategory');
        });
    });

    it('should return an article with httpQuery: { hasComment }', () => {
      const payload = require('../json/article.get')(testContext);

      return wsops
        .get(`/articles/${payload.aid[0]}?hasComment=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.error.text).to.be.equal(undefined);
          expect(response).to.have.status(200);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(ArticleModelKeys);
          expect(response.body).to.not.have.own.property('hasUser');
          expect(response.body).to.not.have.own.property('hasCategory');
          expect(response.body).to.have.own.property('hasComment');
          expect(response.body.hasComment).to.be.an('array');
          expect(response.body.hasComment[0]).to.includes.all.keys(CommentModelKeys);
        });
    });

    it('should return an article with httpQuery: { hasCategory }', () => {
      const payload = require('../json/article.get')(testContext);

      return wsops
        .get(`/articles/${payload.aid[0]}?hasCategory=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.error.text).to.be.equal(undefined);
          expect(response).to.have.status(200);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(ArticleModelKeys);
          expect(response.body).to.not.have.own.property('hasUser');
          expect(response.body).to.not.have.own.property('hasComment');
          expect(response.body).to.have.own.property('hasCategory');
          expect(response.body.hasCategory).to.be.an('array');
          expect(response.body.hasCategory[0]).to.includes.all.keys(CategoryModelKeys);
        });
    });

    it('should return an article with httpQuery: { hasUser }', () => {
      const payload = require('../json/article.get')(testContext);

      return wsops
        .get(`/articles/${payload.aid[0]}?hasUser=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.error.text).to.be.equal(undefined);
          expect(response).to.have.status(200);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(ArticleModelKeys);
          expect(response.body).to.not.have.own.property('hasCategory');
          expect(response.body).to.not.have.own.property('hasComment');
          expect(response.body).to.have.own.property('hasUser');
          expect(response.body.hasUser).to.be.an('object');
          expect(response.body.hasUser).to.not.includes.keys('password');
          expect(response.body.hasUser).to.have.own.property('hasDetails');
          expect(response.body.hasUser.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
          expect(response.body.hasUser).to.have.own.property('hasRole');
          expect(response.body.hasUser.hasRole).to.includes.all.keys(UserRoleModelKeys);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#REPORT', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('Should return an article\'s array without httpQuery: {}', () => wsops
      .request('report', '/articles')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.includes.all.keys(ArticleModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasComments');
        expect(response.body[0]).to.not.have.own.property('hasUser');
        expect(response.body[0]).to.not.have.own.property('hasCategory');
      }));

    it('Should return an article\'s array with httpQuery: { hasComment }', () => wsops
      .request('report', '/articles?hasComment=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.includes.all.keys(ArticleModelKeys);
        expect(response.body[0]).to.have.own.property('hasComment');
        expect(response.body[0].hasComment).to.be.an('array');
        expect(response.body[0].hasComment[0]).to.includes.all.keys(CommentModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasUser');
        expect(response.body[0]).to.not.have.own.property('hasCategory');
      }));

    it('Should return an article\'s array with httpQuery: { hasCategory }', () => wsops
      .request('report', '/articles?hasCategory=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.includes.all.keys(ArticleModelKeys);
        expect(response.body[0]).to.have.own.property('hasCategory');
        expect(response.body[0].hasCategory).to.be.an('array');
        expect(response.body[0].hasCategory[0]).to.includes.all.keys(CategoryModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasUser');
        expect(response.body[0]).to.not.have.own.property('hasComment');
      }));

    it('Should return an article\'s array with httpQuery: { hasUser }', () => wsops
      .request('report', '/articles?hasUser=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.includes.all.keys(ArticleModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasCategory');
        expect(response.body[0]).to.not.have.own.property('hasComment');
        expect(response.body[0]).to.have.own.property('hasUser');
        expect(response.body[0].hasUser).to.be.an('object');
        expect(response.body[0].hasUser).to.not.includes.keys('password');
        expect(response.body[0].hasUser).to.have.own.property('hasDetails');
        expect(response.body[0].hasUser.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        expect(response.body[0].hasUser).to.have.own.property('hasRole');
        expect(response.body[0].hasUser.hasRole).to.includes.all.keys(UserRoleModelKeys);
      }));

    it('Should return an article\'s array with httpQuery: { limit }', () => wsops
      .request('report', '/articles?limit=1')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body.length).to.be.equal(1);
        expect(response.body[0]).to.includes.all.keys(ArticleModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasComment');
        expect(response.body[0]).to.not.have.own.property('hasUser');
        expect(response.body[0]).to.not.have.own.property('hasCategory');
      }));

    it('Should return an article\'s array with httpQuery: { orderByDesc }', () => wsops
      .request('report', '/articles?orderByDesc=aid')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        // expect(response.body[0].aid > response.body[1].aid).to.be.equal(true);
        expect(response.body[0]).to.includes.all.keys(ArticleModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasComments');
        expect(response.body[0]).to.not.have.own.property('hasUser');
        expect(response.body[0]).to.not.have.own.property('hasCategory');
      }));

    it('Should return an article\'s array with httpQuery: { orderByComment }', () => wsops
      .request('report', '/articles?orderByComment=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.includes.all.keys(ArticleModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasComments');
        expect(response.body[0]).to.not.have.own.property('hasUser');
        expect(response.body[0]).to.not.have.own.property('hasCategory');
      }));

    it('Should return a count of articles\'s array with httpQuery: { count }', () => wsops
      .request('report', '/articles?count=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('number');
        expect(response.body).to.not.have.own.property('hasComments');
        expect(response.body).to.not.have.own.property('hasUser');
        expect(response.body).to.not.have.own.property('hasCategory');
      }));

    it('Should return an article\'s array PAGINATED with httpQuery: { page&pageSize }', () => wsops
      .request('report', '/articles?page=0&pageSize=1')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('object');
        expect(response.body).to.haveOwnProperty('total');
        expect(response.body).to.haveOwnProperty('results');
        expect(response.body.results[0]).to.includes.all.keys(ArticleModelKeys);
        expect(response.body.results[0]).to.not.have.own.property('hasComments');
        expect(response.body.results[0]).to.not.have.own.property('hasUser');
        expect(response.body.results[0]).to.not.have.own.property('hasCategory');
      }));

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#POST', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('should insert and fetch a new article without httpQuery: {}', () => {
      const payload = require('../json/article.post')(testContext);

      return wsops
        .post('/articles?testSession=user@test.fr')
        .send(payload)
        .then((response) => {
          console.log(response.body);
          testContext.aids.push(response.body.aid);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(ArticleModelKeys);
          expect(response.body).to.not.have.own.property('hasUser');
          expect(response.body).to.have.own.property('hasCategory');
          expect(response.body.hasCategory[0]).to.includes.all.keys(CategoryModelKeys);
        });
    });

    it('should insert and fetch a new article with httpQuery: { withUser }', () => {
      const payload = require('../json/article.post')(testContext);

      return wsops
        .post('/articles?testSession=user@test.fr&withUser=true')
        .send(payload)
        .then((response) => {
          console.log(response.body);
          testContext.aids.push(response.body.aid);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(ArticleModelKeys);
          expect(response.body).to.have.own.property('hasCategory');
          expect(response.body.hasCategory[0]).to.includes.all.keys(CategoryModelKeys);
          expect(response.body).to.have.own.property('hasUser');
          expect(response.body.hasUser).to.not.includes.keys('password');
          expect(response.body.hasUser).to.have.own.property('hasDetails');
          expect(response.body.hasUser.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
          expect(response.body.hasUser).to.have.own.property('hasRole');
          expect(response.body.hasUser.hasRole).to.includes.all.keys(UserRoleModelKeys);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#PUT', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('should update and return an article', () => {
      const payload = require('../json/article.put')(testContext);

      return wsops
        .put(`/articles/${payload.aid}?testSession=user@test.fr`)
        .send(payload)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(ArticleModelKeys);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#DELETE', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('should delete an article', () => {
      const payload = require('../json/article.get')(testContext);

      return wsops
        .delete(`/articles/${payload.aid}?testSession=user@test.fr`)
        .send(payload)
        .then((response) => {
          console.log(response.status);
          expect(response.status).to.be.equal(200);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });
});
