module.exports = ({ uid, catid }) => ({
  user_uid: uid,
  hasCategory: [{ cid: catid }],
  title: 'Test méthode post',
  body: 'Test de la méthode post sur les articles',
  created_at: '01/12/2021',
});
