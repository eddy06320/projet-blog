const { expect } = require('chai');
const ArticleRulers = require('../../../../src/api/article/article.rulers.js');

describe('[Rulers] article', () => {
  describe('#Get', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('should no return error without httpQuery', () => {
      const payload = require('../json/article.get')(testContext);
      // console.log(payload.aid[0]);
      // eslint-disable-next-line prefer-destructuring
      payload.aid = payload.aid[0];

      return ArticleRulers.Get.validate({ payload })
        .catch((e) => {
          console.log(e);
          expect(e).to.be.equal(undefined);
        });
    });

    it('should no return error with httpQuery: { hasComment }', () => {
      const payload = require('../json/article.get')(testContext);
      // console.log(payload.aid[0]);
      // eslint-disable-next-line prefer-destructuring
      payload.aid = payload.aid[0];

      return ArticleRulers.Get.validate({ payload, httpQuery: { hasComment: true } })
        .catch((e) => {
          console.log(e);
          expect(e).to.be.equal(undefined);
        });
    });

    it('should no return error with httpQuery: { hasComment&hasUser }', () => {
      const payload = require('../json/article.get')(testContext);
      // eslint-disable-next-line prefer-destructuring
      payload.aid = payload.aid[0];

      // eslint-disable-next-line max-len
      return ArticleRulers.Get.validate({ payload, httpQuery: { hasComment: true, hasUser: true } })
        .catch((e) => {
          console.log(e);
          expect(e).to.be.equal(undefined);
        });
    });

    it('should no return error with httpQuery: { hasComment&hasUser&hasCategory }', () => {
      const payload = require('../json/article.get')(testContext);
      // eslint-disable-next-line prefer-destructuring
      payload.aid = payload.aid[0];

      // eslint-disable-next-line max-len
      return ArticleRulers.Get.validate({ payload, httpQuery: { hasComment: true, hasUser: true, hasCategory: true } })
        .catch((e) => {
          console.log(e);
          expect(e).to.be.equal(undefined);
        });
    });

    it('should return a BadRequestError, missing data', () => {
      const payload = require('../json/article.get')(testContext);
      delete payload.aid;

      return ArticleRulers.Get.validate({ payload })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch(({ status }) => {
          expect(status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG aid data', () => {
      const payload = require('../json/article.get')(testContext);
      payload.aid = -1;

      return ArticleRulers.Get.validate({ payload })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch(({ message, status }) => {
          console.log(message);
          expect(status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG httpQuery value', () => {
      const payload = require('../json/article.get')(testContext);
      // eslint-disable-next-line prefer-destructuring
      payload.aid = payload.aid[0];

      return ArticleRulers.Get.validate({ payload, httpQuery: { hasComments: 'chaussette' } })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch(({ message, status }) => {
          console.log(message);
          expect(status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG httpQuery key', () => {
      const payload = require('../json/article.get')(testContext);
      // eslint-disable-next-line prefer-destructuring
      payload.aid = payload.aid[0];

      return ArticleRulers.Get.validate({ payload, httpQuery: { chaussette: true } })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch(({ message, status }) => {
          console.log(message);
          expect(status).to.be.equal(400);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#REPORT', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('shouldnt return error without httpQuery', () => ArticleRulers.Report.validate({ httpQuery: {} })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { limit }', () => ArticleRulers.Report.validate({ httpQuery: { limit: 1 } })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { count }', () => ArticleRulers.Report.validate({ httpQuery: { count: '' } })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { hasComment }', () => ArticleRulers.Report.validate({ httpQuery: { hasComment: true } })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { hasCategory }', () => ArticleRulers.Report.validate({ httpQuery: { hasCategory: true } })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { hasUser }', () => ArticleRulers.Report.validate({ httpQuery: { hasUser: true } })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { orderByDesc }', () => ArticleRulers.Report.validate({ httpQuery: { orderByDesc: 'title' } })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { orderByComment }', () => ArticleRulers.Report.validate({ httpQuery: { orderByComment: true } })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { page&pageSize }', () => ArticleRulers.Report.validate({ httpQuery: { page: 0, pageSize: 1 } })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { hasComment&hasUser }', () => ArticleRulers.Report.validate({ httpQuery: { hasUser: true, hasComment: true } })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('shouldnt return error with httpQuery: { hasComment&hasUser&hasCategory&orderByCom }', () => ArticleRulers.Report.validate({
      httpQuery:
      {
        hasUser: true,
        hasComment: true,
        hasCategory: true,
        orderByComment: true,
      },
    })
      .catch((e) => {
        console.log(e);
        expect(e).to.be.equal(undefined);
      }));

    it('should return BadRequestError with httpQuery: { count&limit }', () => ArticleRulers.Report.validate({ httpQuery: { count: true, limit: 1 } })
      .then(() => {
        throw new Error('error was expected');
      })
      .catch((e) => {
        console.log(e);
        expect(e.status).to.be.equal(400);
      }));

    it('should return BadRequestError with httpQuery: { page&limit }', () => ArticleRulers.Report.validate({ httpQuery: { page: 0, pageSize: 1, limit: 1 } })
      .then(() => {
        throw new Error('error was expected');
      })
      .catch((e) => {
        console.log(e);
        expect(e.status).to.be.equal(400);
      }));

    it('should return BadRequestError with httpQuery: { page&count }', () => ArticleRulers.Report.validate({ httpQuery: { page: 0, pageSize: 1, count: true } })
      .then(() => {
        throw new Error('error was expected');
      })
      .catch((e) => {
        // console.log(e);
        expect(e.status).to.be.equal(400);
      }));

    it('should return BadRequestError WRONG value httpQuery: { limit }', () => ArticleRulers.Report.validate({ httpQuery: { limit: true } })
      .then(() => {
        throw new Error('Success, Rejection was expected');
      })
      .catch((e) => {
        console.log(e);
        expect(e.status).to.be.equal(400);
      }));

    it('should return BadRequestError WRONG value httpQuery: { hasComments }', () => ArticleRulers.Report.validate({ httpQuery: { hasComments: 1 } })
      .then(() => {
        throw new Error('Success, Rejection was expected');
      })
      .catch((e) => {
        console.log(e);
        expect(e.status).to.be.equal(400);
      }));

    it('should return BadRequestError WRONG value httpQuery: { orderByDesc }', () => ArticleRulers.Report.validate({ httpQuery: { orderByDesc: true } })
      .then(() => {
        throw new Error('Success, Rejection was expected');
      })
      .catch((e) => {
        console.log(e);
        expect(e.status).to.be.equal(400);
      }));

    it('should return BadRequestError WRONG value httpQuery: { orderByComment }', () => ArticleRulers.Report.validate({ httpQuery: { orderByComment: 'chaussette' } })
      .then(() => {
        throw new Error('Success, Rejection was expected');
      })
      .catch((e) => {
        console.log(e);
        expect(e.status).to.be.equal(400);
      }));

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#POST', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.post').run(testContext));

    it('shouldnt return error', () => {
      const payload = require('../json/article.post')(testContext);

      return ArticleRulers.Post.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('should return a BadRequestError, missing data', () => {
      const payload = require('../json/article.post')(testContext);
      delete payload.title;

      return ArticleRulers.Post.validate({ payload })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch((e) => {
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG data', () => {
      const payload = require('../json/article.post')(testContext);
      payload.title = 1;

      return ArticleRulers.Post.validate({ payload })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch((e) => {
          expect(e.status).to.be.equal(400);
        });
    });

    after(() => require('../teardown/article.common').run(testContext)); // Le teardown fonctionne alors que l'aid n'est pas merge dans le testContext ???????
  });

  describe('#PUT', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('shouldnt return errors', () => {
      const payload = require('../json/article.put')(testContext);
      // eslint-disable-next-line prefer-destructuring
      payload.aid = payload.aid[0];

      return ArticleRulers.Put.validate({ payload })
        .catch(e => console.log(e));
    });

    it('should return a BadRequestError, missing data', () => {
      const payload = require('../json/article.put')(testContext);
      delete payload.body;

      return ArticleRulers.Put.validate({ payload })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a NotFoundError article aid not exist', () => {
      const payload = require('../json/article.put')(testContext);
      payload.aid = 0;

      return ArticleRulers.Put.validate({ payload })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch((e) => {
          expect(e.message).to.be.equal('Article not found bitches');
          expect(e.status).to.be.equal(404);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#DELETE', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('shouldnt return errors', () => {
      const payload = require('../json/article.get')(testContext);
      // eslint-disable-next-line prefer-destructuring
      payload.aid = payload.aid[0];

      return ArticleRulers.Delete.validate({ payload })
        .catch(e => console.log(e));
    });

    it('should return a BadRequestError when missing required data', () => {
      const payload = require('../json/article.get')(testContext);
      delete payload.aid;

      return ArticleRulers.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a NotFoundError when aid is not found', () => {
      const payload = require('../json/article.get')(testContext);
      payload.aid = 0;

      return ArticleRulers.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch((e) => {
          expect(e.message).to.be.equal('Article not found bitches');
          expect(e.status).to.be.equal(404);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });
});
