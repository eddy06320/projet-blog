const { expect } = require('chai');

const ArticleServices = require('../../../../src/api/article/article.services.js');

const ArticleModel = require('../../../../engine/datadealer/Article');
const CommentModel = require('../../../../engine/datadealer/Comment');
const CategoryModel = require('../../../../engine/datadealer/Category');
const UserRoleModel = require('../../../../engine/datadealer/UserRole');
const UserDetailsModel = require('../../../../engine/datadealer/UserDetails');

const ArticleModelKeys = ArticleModel.tableColumns;
const CommentModelKeys = CommentModel.tableColumns;
const CategoryModelKeys = CategoryModel.tableColums;
const UserRoleModelKeys = UserRoleModel.tableColumns;
const UserDetailsModelKeys = UserDetailsModel.tableColumns;

describe('[Services] Article', () => {
  describe('#GET', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('should return proper dataset, without httpQuery: {}', () => {
      const payload = require('../json/article.get')(testContext);

      return ArticleServices.get({ payload, httpQuery: {} })
        .then((article) => {
          console.log(article);
          expect(article).to.includes.all.keys(ArticleModelKeys);
          expect(article).to.not.have.own.property('hasComments');
          expect(article).to.not.have.own.property('hasCategory');
          expect(article).to.not.have.own.property('hasUser');
        });
    });

    it('should return proper dataset, with httpQuery: { hasComment }', () => {
      const payload = require('../json/article.get')(testContext);

      return ArticleServices.get({ payload, httpQuery: { hasComment: true } })
        .then((article) => {
          console.log(article);
          expect(article).to.be.an('object');
          expect(article).to.includes.all.keys(ArticleModelKeys);
          expect(article).to.not.have.own.property('hasCategory');
          expect(article).to.not.have.own.property('hasUser');
          expect(article).to.have.own.property('hasComment');
          expect(article.hasComment).to.be.an('array');
          expect(article.hasComment[0]).to.includes.all.keys(CommentModelKeys);
        });
    });

    it('should return proper dataset, with httpQuery: { hasCategory }', () => {
      const payload = require('../json/article.get')(testContext);

      return ArticleServices.get({ payload, httpQuery: { hasCategory: true } })
        .then((article) => {
          console.log(article);
          expect(article).to.be.an('object');
          expect(article).to.includes.all.keys(ArticleModelKeys);
          expect(article).to.not.have.own.property('hasComment');
          expect(article).to.not.have.own.property('hasUser');
          expect(article).to.have.own.property('hasCategory');
          expect(article.hasCategory).to.be.an('array');
          expect(article.hasCategory[0]).to.includes.all.keys(CategoryModelKeys);
        });
    });

    it('should return proper dataset, with httpQuery: { hasUser }', () => {
      const payload = require('../json/article.get')(testContext);

      return ArticleServices.get({ payload, httpQuery: { hasUser: true } })
        .then((article) => {
          console.log(article);
          expect(article).to.be.an('object');
          expect(article).to.includes.all.keys(ArticleModelKeys);
          expect(article).to.not.have.own.property('hasComment');
          expect(article).to.not.have.own.property('hasCategory');
          expect(article).to.have.own.property('hasUser');
          expect(article.hasUser).to.be.an('object');
          expect(article.hasUser).to.not.includes.keys('password');
          expect(article.hasUser).to.have.own.property('hasRole');
          expect(article.hasUser.hasRole).to.be.an('object');
          expect(article.hasUser.hasRole).to.includes.all.keys(UserRoleModelKeys);
          expect(article.hasUser).to.have.own.property('hasDetails');
          expect(article.hasUser.hasDetails).to.be.an('object');
          expect(article.hasUser.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        });
    });

    it('should return proper dataset, with httpQuery: { hasUser&hasComment&hasCategory }', () => {
      const payload = require('../json/article.get')(testContext);

      // eslint-disable-next-line max-len
      return ArticleServices.get({ payload, httpQuery: { hasUser: true, hasComment: true, hasCategory: true } })
        .then((article) => {
          console.log(article);
          expect(article).to.be.an('object');
          expect(article).to.includes.all.keys(ArticleModelKeys);
          expect(article).to.have.own.property('hasComment');
          expect(article.hasComment).to.be.an('array');
          expect(article.hasComment[0]).to.includes.all.keys(CommentModelKeys);
          expect(article).to.have.own.property('hasCategory');
          expect(article.hasCategory).to.be.an('array');
          expect(article.hasCategory[0]).to.includes.all.keys(CategoryModelKeys);
          expect(article).to.have.own.property('hasUser');
          expect(article.hasUser).to.be.an('object');
          expect(article.hasUser).to.not.includes.keys('password');
          expect(article.hasUser).to.have.own.property('hasRole');
          expect(article.hasUser.hasRole).to.be.an('object');
          expect(article.hasUser.hasRole).to.includes.all.keys(UserRoleModelKeys);
          expect(article.hasUser).to.have.own.property('hasDetails');
          expect(article.hasUser.hasDetails).to.be.an('object');
          expect(article.hasUser.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#REPORT', () => {
    const testContext = { aids: [] };
    const limit = 1;
    const page = 0;
    const pageSize = 1;

    before(() => require('../setup/article.common').run(testContext));

    it('should return array of articles without httpQuery: {}', () => ArticleServices.report({ httpQuery: {} })
      .then((articles) => {
        console.log(articles);
        expect(articles).to.be.an('array');
        expect(articles[0]).to.be.an('object');
        expect(articles[0]).to.includes.all.keys(ArticleModelKeys);
        expect(articles[0]).to.not.have.own.property('hasCategory');
        expect(articles[0]).to.not.have.own.property('hasComments');
        expect(articles[0]).to.not.have.own.property('hasUser');
      }));

    it('should return array of articles with httpQuery: { hasComment }', () => ArticleServices.report({ httpQuery: { hasComment: true } })
      .then((articles) => {
        console.log(articles);
        expect(articles).to.be.an('array');
        expect(articles[0]).to.be.an('object');
        expect(articles[0]).to.includes.all.keys(ArticleModelKeys);
        expect(articles[0]).to.have.own.property('hasComment');
        expect(articles[0].hasComment).to.be.an('array');
        expect(articles[0].hasComment[0]).to.includes.all.keys(CommentModelKeys);
        expect(articles[0]).to.not.have.own.property('hasCategory');
        expect(articles[0]).to.not.have.own.property('hasUser');
      }));

    it('should return array of articles with httpQuery: { hasCategory }', () => ArticleServices.report({ httpQuery: { hasCategory: true } })
      .then((articles) => {
        console.log(articles);
        expect(articles).to.be.an('array');
        expect(articles[0]).to.be.an('object');
        expect(articles[0]).to.includes.all.keys(ArticleModelKeys);
        expect(articles[0]).to.have.own.property('hasCategory');
        expect(articles[0].hasCategory).to.be.an('array');
        expect(articles[0].hasCategory[0]).to.includes.all.keys(CategoryModelKeys);
        expect(articles[0]).to.not.have.own.property('hasComments');
        expect(articles[0]).to.not.have.own.property('hasUser');
      }));

    it('should return array of articles with httpQuery: { hasUser }', () => ArticleServices.report({ httpQuery: { hasUser: true } })
      .then((articles) => {
        console.log(articles);
        expect(articles).to.be.an('array');
        expect(articles[0]).to.be.an('object');
        expect(articles[0]).to.includes.all.keys(ArticleModelKeys);
        expect(articles[0]).to.have.own.property('hasUser');
        expect(articles[0].hasUser).to.be.an('object');
        expect(articles[0].hasUser).to.not.includes.all.keys('password');
        expect(articles[0].hasUser).to.have.own.property('hasDetails');
        expect(articles[0].hasUser.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        expect(articles[0].hasUser).to.have.own.property('hasRole');
        expect(articles[0].hasUser.hasRole).to.includes.all.keys(UserRoleModelKeys);
        expect(articles[0]).to.not.have.own.property('hasComment');
        expect(articles[0]).to.not.have.own.property('hasCategory');
      }));

    it('should return array of articles with httpQuery: { limit }', () => ArticleServices.report({ httpQuery: { limit } })
      .then((articles) => {
        console.log(articles);
        expect(articles).to.be.an('array');
        // Test if articles.length === { httpQuery: { limit } }
        expect(articles.length).to.be.equal(1);
        expect(articles[0]).to.be.an('object');
        expect(articles[0]).to.includes.all.keys(ArticleModelKeys);
        expect(articles[0]).to.not.have.own.property('hasComments');
        expect(articles[0]).to.not.have.own.property('hasCategory');
        expect(articles[0]).to.not.have.own.property('hasUser');
      }));

    it('should return array of articles with httpQuery: { orderByDesc }', () => ArticleServices.report({ httpQuery: { orderByDesc: 'aid' } })
      .then((articles) => {
        console.log(articles);
        expect(articles).to.be.an('array');
        expect(articles[0]).to.be.an('object');
        expect(articles[0]).to.includes.all.keys(ArticleModelKeys);
        expect(articles[0]).to.not.have.own.property('hasCategory');
        expect(articles[0]).to.not.have.own.property('hasComments');
        expect(articles[0]).to.not.have.own.property('hasUser');
      }));

    it('should return array of articles with httpQuery: { orderByComment }', () => ArticleServices.report({ httpQuery: { orderByComment: true } })
      .then((articles) => {
        console.log(articles);
        expect(articles).to.be.an('array');
        expect(articles[0]).to.be.an('object');
        expect(articles[0]).to.includes.all.keys('numberOfComments');
        // add test to check if articles[0].numberOfComments >= articles[1,2,3...].numberOfComments
        expect(articles[0]).to.includes.all.keys(ArticleModelKeys);
        expect(articles[0]).to.not.have.own.property('hasCategory');
        expect(articles[0]).to.not.have.own.property('hasComments');
        expect(articles[0]).to.not.have.own.property('hasUser');
      }));

    it('should an array of articles count with httpQuery: { count }', () => ArticleServices.report({ httpQuery: { count: true } })
      .then((articleCount) => {
        console.log(articleCount);
        expect(articleCount).to.be.an('number');
        expect(articleCount).to.not.includes.all.keys(ArticleModelKeys);
        expect(articleCount).to.not.have.own.property('hasCategory');
        expect(articleCount).to.not.have.own.property('hasComments');
        expect(articleCount).to.not.have.own.property('hasUser');
      }));

    it('should return array of articles with httpQuery: { page&pageSize }', () => ArticleServices.report({ httpQuery: { page, pageSize } })
      .then((articles) => {
        console.log(articles);
        expect(articles).to.be.an('object');
        expect(articles).to.have.own.property('total');
        expect(articles).to.have.own.property('results');
        expect(articles.results).to.be.an('array');
        expect(articles.results.length).to.be.equal(pageSize);
        expect(articles.results[0]).to.be.an('object');
        expect(articles.results[0]).to.includes.all.keys(ArticleModelKeys);
        expect(articles.results[0]).to.not.have.own.property('hasComments');
        expect(articles.results[0]).to.not.have.own.property('hasCategory');
        expect(articles.results[0]).to.not.have.own.property('hasUser');
      }));

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#POST', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.post').run(testContext));

    it('should insert an article and return proper dataset', () => {
      const payload = require('../json/article.post')(testContext);

      return ArticleServices.create({ payload })
        .then((article) => {
          console.log(article);
          testContext.article = article;
          testContext.aids.push(article.aid);
          expect(article).to.be.an('object');
          expect(article).to.includes.all.keys(ArticleModelKeys);
          expect(article).to.have.own.property('hasCategory');
          expect(article.hasCategory[0]).to.includes.all.keys(CategoryModelKeys);
        });
    });

    it('should insert an article and return proper dataset with user information', () => {
      const payload = require('../json/article.post')(testContext);

      return ArticleServices.create({ payload, httpQuery: { withUser: true } })
        .then((article) => {
          console.log(article);
          testContext.article = article;
          testContext.aids.push(article.aid);
          expect(article).to.be.an('object');
          expect(article).to.includes.all.keys(ArticleModelKeys);
          expect(article).to.have.own.property('hasCategory');
          expect(article.hasCategory[0]).to.includes.all.keys(CategoryModelKeys);
          expect(article).to.have.own.property('hasUser');
          expect(article.hasUser).to.not.includes.keys('password');
          expect(article.hasUser).to.have.own.property('hasDetails');
          expect(article.hasUser.hasDetails).to.includes.keys(UserDetailsModelKeys);
          expect(article.hasUser).to.have.own.property('hasRole');
          expect(article.hasUser.hasRole).to.includes.keys(UserRoleModelKeys);
          console.log(testContext);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#PUT', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('should update an article and return proper dataset', () => {
      const payload = require('../json/article.put')(testContext);
      // console.log(testContext);
      payload.aid = 935;
      // console.log(payload);
      return ArticleServices.update({ payload })
        .then((article) => {
          console.log(article);
          expect(article).to.be.an('object');
          expect(article).to.includes.all.keys(ArticleModelKeys);
        });
    });
    after(() => require('../teardown/article.common').run(testContext));
  });

  describe('#DELETE', () => {
    const testContext = { aids: [] };

    before(() => require('../setup/article.common').run(testContext));

    it('should delete proper dataset', () => {
      const payload = require('../json/article.get')(testContext);

      return ArticleServices.delete({ payload })
        .then((res) => {
          console.log(res);
          expect(res).to.be.equal(1);
        });
    });

    after(() => require('../teardown/article.common').run(testContext));
  });
});
