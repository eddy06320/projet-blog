const { expect } = require('chai');
const ContactRuler = require('../../../../src/api/contact/contact.rulers');

describe('[Rulers] Contact', () => {
  describe('#POSTMAIL', () => {
    it('shouldnt return errors', () => {
      const payload = require('../json/contact');

      return ContactRuler.Post.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('shouldnt return a BadRequestError, MISSING data', () => {
      const payload = require('../json/contact');
      delete payload.name;

      return ContactRuler.Post.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('shouldnt return a BadRequestError, WRONG data', () => {
      const payload = require('../json/contact');
      payload.name = 1;

      return ContactRuler.Post.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });
  });
});
