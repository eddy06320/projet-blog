const { expect } = require('chai');

describe('[Routes] Contact', () => {
  describe('#POSTMAIL', () => {
    it('should send an Email', () => {
      const payload = require('../json/contact');
      payload.name = 'toto';
      return wsops
        .post('/contact')
        .send(payload)
        .then((response) => {
          console.log(response.body);
          expect(response.body.message).to.be.equal('Success');
          expect(response.status).to.be.equal(200);
        });
    });
  });
});
