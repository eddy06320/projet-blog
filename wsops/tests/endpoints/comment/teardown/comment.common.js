const Action = require('../../../utils/Action');
const database = require('../../../../engine/knex');

module.exports = class CommentTearDown extends Action {
  action = {
    deleteArticle () {
      const { aid } = this.context();

      if (!aid) return undefined;

      return database('article')
        .delete()
        .where({ aid });
    },
    deleteComment () {
      const { cid } = this.context();

      return database('comment')
        .delete()
        .where({ cid });
    },
    deleteUser () {
      const { uid } = this.context();

      return database('user')
        .delete()
        .where({ uid });
    },
    deleteUserRole () {
      const { uid } = this.context();
      if (!uid) return undefined;

      return database('userRole')
        .delete()
        .where({ userUid: uid });
    },
    deleteUserDetails () {
      const { uid } = this.context();
      if (!uid) return undefined;

      return database('userDetails')
        .delete()
        .where({ userUid: uid });
    },
  }
};
