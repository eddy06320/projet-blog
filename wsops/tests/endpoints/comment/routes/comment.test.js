const { expect } = require('chai');

const ArticleModel = require('../../../../engine/datadealer/Article');
const CommentModel = require('../../../../engine/datadealer/Comment');

const ArticleModelKeys = ArticleModel.tableColumns;
const CommentModelKeys = CommentModel.tableColumns;

describe('[ROUTES] comment', () => {
  describe('#GET', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('should return a comment without httpQuery: {}', () => {
      const payload = require('../json/comment.get')(testContext);

      return wsops
        .get(`/comments/${payload.cid}`)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(CommentModelKeys);
          expect(response.body).to.not.have.own.property('hasArticle');
          expect(response.body).to.not.have.own.property('hasUser');
        });
    });

    it('should return a comment with httpQuery: { hasArticle }', () => {
      const payload = require('../json/comment.get')(testContext);

      return wsops
        .get(`/comments/${payload.cid}?hasArticle=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(CommentModelKeys);
          expect(response.body).to.have.own.property('hasArticle');
          expect(response.body.hasArticle).to.be.an('object');
          expect(response.body.hasArticle).to.includes.all.keys(ArticleModelKeys);
          expect(response.body).to.not.have.own.property('hasUser');
        });
    });

    it('should return a comment with httpQuery: { hasUser }', () => {
      const payload = require('../json/comment.get')(testContext);

      return wsops
        .get(`/comments/${payload.cid}?hasUser=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(CommentModelKeys);
          expect(response.body).to.have.own.property('hasUser');
          expect(response.body.hasUser).to.be.an('object');
          expect(response.body.hasUser).to.not.includes.keys('password');
          expect(response.body).to.not.have.own.property('hasArticle');
        });
    });

    it('should return a comment with httpQuery: { hasUser&hasArticle }', () => {
      const payload = require('../json/comment.get')(testContext);

      return wsops
        .get(`/comments/${payload.cid}?hasUser=true&hasArticle=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(CommentModelKeys);
          expect(response.body).to.have.own.property('hasUser');
          expect(response.body.hasUser).to.be.an('object');
          expect(response.body.hasUser).to.not.includes.keys('password');
          expect(response.body).to.have.own.property('hasArticle');
          expect(response.body.hasArticle).to.be.an('object');
          expect(response.body.hasArticle).to.includes.all.keys(ArticleModelKeys);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#REPORT', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('should return a comment\'s array without httpQuery: {}', () => wsops
      .request('report', '/comments')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.includes.all.keys(CommentModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasArticle');
      }));

    it('should return a comment\'s array with httpQuery: { hasArticle }', () => wsops
      .request('report', '/comments?hasArticle=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.includes.all.keys(CommentModelKeys);
        expect(response.body[0]).to.have.own.property('hasArticle');
        expect(response.body[0].hasArticle).to.be.an('object');
        expect(response.body[0].hasArticle).to.includes.all.keys(ArticleModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasUser');
      }));

    it('should return a comment\'s array with httpQuery: { hasUser }', () => wsops
      .request('report', '/comments?hasUser=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.includes.all.keys(CommentModelKeys);
        expect(response.body[0]).to.have.own.property('hasUser');
        expect(response.body[0].hasUser).to.be.an('object');
        expect(response.body[0].hasUser).to.not.includes.keys('password');
        expect(response.body[0]).to.not.have.own.property('hasArticle');
      }));

    it('should return a comment\'s array with httpQuery: { hasUser&hasArticle }', () => wsops
      .request('report', '/comments?hasUser=true&hasArticle')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.includes.all.keys(CommentModelKeys);
        expect(response.body[0]).to.have.own.property('hasUser');
        expect(response.body[0].hasUser).to.be.an('object');
        expect(response.body[0].hasUser).to.not.includes.keys('password');
        expect(response.body[0]).to.have.own.property('hasArticle');
        expect(response.body[0].hasArticle).to.be.an('object');
        expect(response.body[0].hasArticle).to.includes.all.keys(ArticleModelKeys);
      }));

    it('should return a comment\'s array with httpQuery: { count }', () => wsops
      .request('report', '/comments?count=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('number');
        expect(response.body).to.not.have.own.property('hasUser');
        expect(response.body).to.not.have.own.property('hasArticle');
      }));

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#POST', () => {
    const testContext = {};

    before(() => require('../setup/comment.post').run(testContext));

    it('should insert and return a comment', () => {
      const payload = require('../json/comment.post')(testContext);

      return wsops
        .post('/comments?testSession=user@test.fr')
        .send(payload)
        .then((response) => {
          console.log(response.body);
          testContext.cid = response.body.cid;

          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(CommentModelKeys);
          expect(response.body).to.have.own.property('hasArticle');
          expect(response.body.hasArticle).to.be.an('object');
          expect(response.body.hasArticle).to.includes.all.keys(ArticleModelKeys);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#PUT', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('should update a comment', () => {
      const payload = require('../json/comment.put')(testContext);

      return wsops
        .put(`/comments/${payload.cid}?testSession=user@test.fr`)
        .send(payload)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(CommentModelKeys);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#DELETE', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('should delete a comment', () => {
      const payload = require('../json/comment.get')(testContext);

      return wsops
        .delete(`/comments/${payload.cid}?testSession=user@test.fr`)
        .then((response) => {
          expect(response.status).to.be.equal(200);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });
});
