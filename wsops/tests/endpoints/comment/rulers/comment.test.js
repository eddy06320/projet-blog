const { expect } = require('chai');
const CommentRulers = require('../../../../src/api/comments/comments.rulers');

describe('[Rulers] comment', () => {
  describe('#GET', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('shouldnt return error without httpQuery', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentRulers.Get.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e).to.be.equal(undefined);
        });
    });

    it('shouldnt return error with httpQuery: { hasArticle }', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentRulers.Get.validate({ payload, httpQuery: { hasArticle: true } })
        .catch((e) => {
          console.log(e.message);
          expect(e).to.be.equal(undefined);
        });
    });

    it('shouldnt return error with httpQuery: { hasUser }', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentRulers.Get.validate({ payload, httpQuery: { hasUser: true } })
        .catch((e) => {
          console.log(e.message);
          expect(e).to.be.equal(undefined);
        });
    });

    it('shouldnt return error with httpQuery: { hasArticle&hasUser }', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentRulers.Get.validate({ payload, httpQuery: { hasArticle: true, hasUser: true } })
        .catch((e) => {
          console.log(e.message);
          expect(e).to.be.equal(undefined);
        });
    });

    it('should return a BadRequestError, missing data', () => {
      const payload = require('../json/comment.get')(testContext);
      delete payload.cid;

      return CommentRulers.Get.validate({ payload })
        .then(() => {
          throw new Error('Success, Rejection was expected');
        })
        .catch(({ status }) => {
          expect(status).to.be.equal(400);
        });
    });

    it('should return BadRequestError, WRONG httpQuery key', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentRulers.Get.validate({ payload, httpQuery: { chaussette: true } })
        .then(() => {
          throw new Error('error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should BadRequestError, WRONG httpQuery value', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentRulers.Get.validate({ payload, httpQuery: { hasArticle: 'e' } })
        .then(() => {
          throw new Error('error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#REPORT', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('shoundnt return error without httpQuery', () => CommentRulers.Report.validate({ httpQuery: {} })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldnt return a error with httpQuery: { hasArticle }', () => CommentRulers.Report.validate({ httpQuery: { hasArticle: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldnt return a error with httpQuery: { hasUser }', () => CommentRulers.Report.validate({ httpQuery: { hasUser: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldnt return a error with httpQuery: { count }', () => CommentRulers.Report.validate({ httpQuery: { count: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldnt return a error with httpQuery: { hasArticle&hasUser }', () => CommentRulers.Report.validate({ httpQuery: { hasArticle: true, hasUser: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('should return a BadRequestError with httpQuery: { hasArticle&count }', () => CommentRulers.Report.validate({ httpQuery: { hasArticle: true, count: true } })
      .then(() => {
        throw new Error('error was expected');
      })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(400);
      }));

    it('should return a BadRequestError with httpQuery: { hasUser&count }', () => CommentRulers.Report.validate({ httpQuery: { hasUser: true, count: true } })
      .then(() => {
        throw new Error('error was expected');
      })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(400);
      }));

    it('should return a BadRequestError with httpQuery WRONG key', () => CommentRulers.Report.validate({ httpQuery: { chaussette: true } })
      .then(() => {
        throw new Error('error was expected');
      })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(400);
      }));

    it('should return a BadRequestError with httpQuery WRONG value', () => CommentRulers.Report.validate({ httpQuery: { hasArticle: 'e' } })
      .then(() => {
        throw new Error('error was expected');
      })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(400);
      }));

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#POST', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('shoundnt return error', () => {
      const payload = require('../json/comment.post')(testContext);

      return CommentRulers.Post.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('should return a BadRequestError, MISSING data', () => {
      const payload = require('../json/comment.post')(testContext);
      delete payload.title;

      return CommentRulers.Post.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG data', () => {
      const payload = require('../json/comment.post')(testContext);
      payload.title = 1;

      return CommentRulers.Post.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#PUT', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('shouldnt return error', () => {
      const payload = require('../json/comment.put')(testContext);

      return CommentRulers.Put.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('should return a BadRequestError, MISSING data', () => {
      const payload = require('../json/comment.put')(testContext);
      delete payload.title;

      return CommentRulers.Put.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG data', () => {
      const payload = require('../json/comment.put')(testContext);
      payload.title = 1;

      return CommentRulers.Put.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a NotFoundError', () => {
      const payload = require('../json/comment.put')(testContext);
      payload.cid = 0;

      return CommentRulers.Put.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          expect(e.message).to.be.equal('Comment not found');
          expect(e.status).to.be.equal(404);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#DELETE', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('shouldnt return error', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentRulers.Delete.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('should return a BadRequestError, MISSING data', () => {
      const payload = require('../json/comment.get')(testContext);
      delete payload.cid;

      return CommentRulers.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG data', () => {
      const payload = require('../json/comment.get')(testContext);
      payload.cid = 'hello';

      return CommentRulers.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a NotFoundError', () => {
      const payload = require('../json/comment.get')(testContext);
      payload.cid = 0;

      return CommentRulers.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          expect(e.message).to.be.equal('Comment not found');
          expect(e.status).to.be.equal(404);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });
});
