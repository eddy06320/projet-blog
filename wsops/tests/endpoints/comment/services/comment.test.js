const { expect } = require('chai');

const CommentServices = require('../../../../src/api/comments/comments.services.js');

const CommentModel = require('../../../../engine/datadealer/Comment');
const ArticleModel = require('../../../../engine/datadealer/Article');
const UserDetailsModel = require('../../../../engine/datadealer/UserDetails');
const UserRoleModel = require('../../../../engine/datadealer/UserRole');
// const UserModel = require('../../../../engine/datadealer/User');

const CommentModelKeys = CommentModel.tableColumns;
const ArticleModelKeys = ArticleModel.tableColumns;
const UserDetailsModelKeys = UserDetailsModel.tableColumns;
const UserRoleModelKeys = UserRoleModel.tableColumns;
// const UserModelKeys = UserModel.tableColumns;

describe('[Services] Comment', () => {
  describe('#GET', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('should return a comment with proper dataset without httpQuery: {}', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentServices.get({ payload, httpQuery: {} })
        .then((comment) => {
          console.log(comment);
          expect(comment).to.be.an('object');
          expect(comment).to.includes.all.keys(CommentModelKeys);
          expect(comment).to.not.have.own.property('hasArticle');
          expect(comment).to.not.have.own.property('hasUser');
        });
    });

    it('should return a comment with httpQuery: { hasArticle }', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentServices.get({ payload, httpQuery: { hasArticle: true } })
        .then((comment) => {
          console.log(comment);
          expect(comment).to.be.an('object');
          expect(comment).to.includes.all.keys(CommentModelKeys);
          expect(comment).to.have.own.property('hasArticle');
          expect(comment.hasArticle).to.be.an('object');
          expect(comment.hasArticle).to.includes.all.keys(ArticleModelKeys);
          expect(comment).to.not.have.own.property('hasUser');
        });
    });

    it('should return a comment with proper dataset with httpQuery: { hasUser }', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentServices.get({ payload, httpQuery: { hasUser: true } })
        .then((comment) => {
          console.log(comment);
          expect(comment).to.be.an('object');
          expect(comment).to.includes.all.keys(CommentModelKeys);
          expect(comment).to.not.have.own.property('hasArticle');
          expect(comment).to.have.own.property('hasUser');
          expect(comment.hasUser).to.be.an('object');
          expect(comment.hasUser).to.not.includes.keys('password');
          expect(comment.hasUser).to.have.own.property('hasDetails');
          expect(comment.hasUser.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
          expect(comment.hasUser).to.have.own.property('hasRole');
          expect(comment.hasUser.hasRole).to.includes.all.keys(UserRoleModelKeys);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#REPORT', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('should return array of comments', () => CommentServices.report({ httpQuery: {} })
      .then((comments) => {
        console.log(comments);
        expect(comments).to.be.an('array');
        expect(comments[0]).to.be.an('object');
        expect(comments[0]).to.includes.all.keys(CommentModelKeys);
        expect(comments[0]).to.not.have.own.property('hasArticle');
        expect(comments[0]).to.not.have.own.property('hasUser');
      }));

    it('should return array of comments with httpQuery: { hasArticle }', () => CommentServices.report({ httpQuery: { hasArticle: true } })
      .then((comments) => {
        console.log(comments);
        expect(comments).to.be.an('array');
        expect(comments[0]).to.be.an('object');
        expect(comments[0]).to.includes.all.keys(CommentModelKeys);
        expect(comments[0]).to.not.have.own.property('hasUser');
        expect(comments[0]).to.have.own.property('hasArticle');
        expect(comments[0].hasArticle).to.be.an('object');
        expect(comments[0].hasArticle).to.includes.all.keys(ArticleModelKeys);
      }));

    it('should return array of comments with httpQuery: { hasUser }', () => CommentServices.report({ httpQuery: { hasUser: true } })
      .then((comments) => {
        console.log(comments);
        expect(comments).to.be.an('array');
        expect(comments[0]).to.be.an('object');
        expect(comments[0]).to.includes.all.keys(CommentModelKeys);
        expect(comments[0]).to.not.have.own.property('hasArticle');
        expect(comments[0]).to.have.own.property('hasUser');
        expect(comments[0].hasUser).to.be.an('object');
        expect(comments[0].hasUser).to.not.includes.keys('password');
        expect(comments[0].hasUser).to.have.own.property('hasDetails');
        expect(comments[0].hasUser.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        expect(comments[0].hasUser).to.have.own.property('hasRole');
        expect(comments[0].hasUser.hasRole).to.includes.all.keys(UserRoleModelKeys);
      }));

    it('should return a comment count with httpQuery: { count }', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentServices.report({ payload, httpQuery: { count: true } })
        .then((count) => {
          console.log(count);
          expect(count).to.be.an('number');
          expect(count).to.not.includes.all.keys(CommentModelKeys);
          expect(count).to.not.have.own.property('hasArticle');
          expect(count).to.not.have.own.property('hasUser');
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#POST', () => {
    const testContext = {};

    before(() => require('../setup/comment.post').run(testContext));

    it('should insert and fetch a new comment with userDetail relation', () => {
      console.log(testContext);
      const payload = require('../json/comment.post')(testContext);

      return CommentServices.create({ payload })
        .then((comment) => {
          console.log(comment);
          testContext.cid = comment.cid;

          expect(comment).to.be.an('object');
          expect(comment).to.includes.all.keys(CommentModelKeys);
          expect(comment).to.have.own.property('hasArticle');
          expect(comment.hasArticle).to.includes.all.keys(ArticleModelKeys);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#PUT', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));

    it('should update and return proper dataset', () => {
      const payload = require('../json/comment.put')(testContext);

      return CommentServices.update({ payload })
        .then((comment) => {
          console.log(comment);
          expect(comment).to.be.an('object');
          expect(comment).to.includes.all.keys(CommentModelKeys);
          expect(comment.title).to.be.equal('modification d\'un commentaire');
          expect(comment.body).to.be.equal('modification d\'un commentaire');
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });

  describe('#DELETE', () => {
    const testContext = {};

    before(() => require('../setup/comment.common').run(testContext));
    it('sould delete proper dataset', () => {
      const payload = require('../json/comment.get')(testContext);

      return CommentServices.delete({ payload })
        .then((res) => {
          console.log(res);
          expect(res).to.be.equal(1);
        });
    });

    after(() => require('../teardown/comment.common').run(testContext));
  });
});
