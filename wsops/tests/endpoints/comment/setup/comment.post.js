const Action = require('../../../utils/Action');

const User = require('../../../../engine/datadealer/User');
const UserDetails = require('../../../../engine/datadealer/UserDetails');
const UserRole = require('../../../../engine/datadealer/UserRole');
const Article = require('../../../../engine/datadealer/Article');

const {
  articleData,
  userData,
  userDetailsData,
  userRoleData,
} = require('./json/comment.common');

module.exports = class ArticleSetup extends Action {
  action = {
    insertUser () {
      const context = this.context();
      const dataSet = userData(context);
      const query = User.query()
        .insertAndFetch(dataSet);

      return query
        .then((user) => {
          // console.log(user);
          context.uid = user.uid;
          context.email = user.email;
        });
    },
    insertUserDetails () {
      const context = this.context();
      const dataSet = userDetailsData(context);
      const query = UserDetails.query()
        .insertAndFetch(dataSet);

      return query
        .then((details) => {
          // console.log(details);
          context.pseudo = details.pseudo;
        });
    },
    insertUserRole () {
      const context = this.context();
      const dataSet = userRoleData(context);
      const query = UserRole.query()
        .insertAndFetch(dataSet);

      return query
        .then((userRole) => {
          context.role = userRole.role;
        });
    },
    insertArticle () {
      const context = this.context();
      const dataSet = articleData(context);
      const query = Article.query()
        .insertAndFetch(dataSet);

      return query
        .then((article) => {
          context.aid = article.aid;
        });
    },
  }
};
