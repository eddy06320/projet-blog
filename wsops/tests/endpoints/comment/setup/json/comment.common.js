const bcrypt = require('bcrypt');

const userData = () => ({
  email: 'test@test.fr',
  password: bcrypt.hashSync('test', 10),
});

const userDetailsData = ({ uid }) => ({
  userUid: uid,
  lname: 'Prenom',
  fname: 'Nom',
  pseudo: 'Pseudo',
});

const userRoleData = ({ uid }) => ({
  userUid: uid,
  role: 'admin',
});

const articleData = ({ uid }) => ({
  user_uid: uid,
  title: 'testingTitle',
  body: 'testingBody',
  created_at: '01/12/2021',
});

const commentData = ({ aid, uid }) => ({
  article_aid: aid,
  user_uid: uid,
  title: 'TestTitleComment',
  body: 'TestBodyComment',
  created_at: '01/12/2021',
});

module.exports = {
  articleData,
  commentData,
  userData,
  userDetailsData,
  userRoleData,
};
