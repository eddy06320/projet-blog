module.exports = () => ({
  email: 'test@test.fr',
  password: 'motdepasse',
  hasDetails: { lname: 'PostPrenom', fname: 'Nom', pseudo: 'Postpseudo' },
  hasRole: { role: 'guest' },
});
