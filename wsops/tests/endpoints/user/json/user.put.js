module.exports = ({ uid, email }) => ({
  uid,
  email,
  hasDetails: {
    fname: 'UpdateNom',
    lname: 'UPrenom',
    pseudo: 'Upseudo',
  },
});
