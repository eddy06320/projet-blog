const { expect } = require('chai');

const UserDetailsModel = require('../../../../engine/datadealer/UserDetails');
const UserRoleModel = require('../../../../engine/datadealer/UserRole');

const UserDetailsModelKeys = UserDetailsModel.tableColumns;
const UserRoleModelKeys = UserRoleModel.tableColumns;

describe('[Routes] user', () => {
  describe('#Get', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('should return a user, without httpQuery: {}', () => {
      const payload = require('../json/user.get')(testContext);

      return wsops
        .get(`/user/${payload.uid}?testSession=user@test.fr`)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.not.includes.keys('password');
          expect(response.body).to.not.have.own.property('hasRole');
          expect(response.body).to.not.have.own.property('hasDetails');
        });
    });

    it('should return a user, with httpQuery: { hasRole }', () => {
      const payload = require('../json/user.get')(testContext);

      return wsops
        .get(`/user/${payload.uid}?testSession=user@test.fr&hasRole=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.not.includes.keys('password');
          expect(response.body).to.have.own.property('hasRole');
          expect(response.body.hasRole).to.be.an('object');
          expect(response.body.hasRole).to.includes.all.keys(UserRoleModelKeys);
          expect(response.body).to.not.have.own.property('hasDetails');
        });
    });

    it('should return a user, with httpQuery: { hasDetails }', () => {
      const payload = require('../json/user.get')(testContext);

      return wsops
        .get(`/user/${payload.uid}?testSession=user@test.fr&hasDetails=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.not.includes.keys('password');
          expect(response.body).to.have.own.property('hasDetails');
          expect(response.body.hasDetails).to.be.an('object');
          expect(response.body.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
          expect(response.body).to.not.have.own.property('hasRole');
        });
    });

    it('should return a user, with httpQuery: { hasRole & hasDetails }', () => {
      const payload = require('../json/user.get')(testContext);

      return wsops
        .get(`/user/${payload.uid}?testSession=user@test.fr&hasDetails=true&hasRole=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.not.includes.keys('password');
          expect(response.body).to.have.own.property('hasDetails');
          expect(response.body.hasDetails).to.be.an('object');
          expect(response.body.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
          expect(response.body).to.have.own.property('hasRole');
          expect(response.body.hasRole).to.be.an('object');
          expect(response.body.hasRole).to.includes.all.keys(UserRoleModelKeys);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#Report', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('should return a array of user, without httpQuery: {}', () => wsops
      .request('report', '/user?testSession=user@test.fr')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.not.includes.keys('password');
        expect(response.body[0]).to.not.have.own.property('hasRole');
        expect(response.body[0]).to.not.have.own.property('hasDetails');
      }));

    it('should return a array of user, with httpQuery: { hasRole }', () => wsops
      .request('report', '/user?testSession=user@test.fr&hasRole=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.not.includes.keys('password');
        expect(response.body[0]).to.not.have.own.property('hasDetails');
        expect(response.body[0]).to.have.own.property('hasRole');
        expect(response.body[0].hasRole).to.be.an('object');
        expect(response.body[0].hasRole).to.includes.all.keys(UserRoleModelKeys);
      }));

    it('should return a array of user, with httpQuery: { hasDetails }', () => wsops
      .request('report', '/user?testSession=user@test.fr&hasDetails=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.not.includes.keys('password');
        expect(response.body[0]).to.not.have.own.property('hasRole');
        expect(response.body[0]).to.have.own.property('hasDetails');
        expect(response.body[0].hasDetails).to.be.an('object');
        expect(response.body[0].hasDetails).to.includes.all.keys(UserDetailsModelKeys);
      }));

    it('should return a array of user, with httpQuery: { hasRole & hasDetails }', () => wsops
      .request('report', '/user?testSession=user@test.fr&hasDetails=true&hasRole=true')
      .then((response) => {
        console.log(response.body);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.not.includes.keys('password');
        expect(response.body[0]).to.have.own.property('hasDetails');
        expect(response.body[0].hasDetails).to.be.an('object');
        expect(response.body[0].hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        expect(response.body[0]).to.have.own.property('hasRole');
        expect(response.body[0].hasRole).to.be.an('object');
        expect(response.body[0].hasRole).to.includes.all.keys(UserRoleModelKeys);
      }));

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#Post', () => {
    const testContext = {};

    it('should insert&fetch a new user', () => {
      const payload = require('../json/user.post')(testContext);

      return wsops
        .post('/user')
        .send(payload)
        .then((response) => {
          console.log(response.body);
          testContext.uid = response.body.uid;

          expect(response.body).to.be.an('object');
          expect(response.body).to.not.includes.keys('password');
          expect(response.body).to.have.own.property('hasRole');
          expect(response.body.hasRole).to.be.an('object');
          expect(response.body.hasRole).to.includes.all.keys(UserRoleModelKeys);
          expect(response.body).to.have.own.property('hasDetails');
          expect(response.body.hasDetails).to.be.an('object');
          expect(response.body.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#Put', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('should update&fetch a user', () => {
      const payload = require('../json/user.put')(testContext);

      return wsops
        .put(`/user/${payload.uid}?testSession=user@test.fr`)
        .send(payload)
        .then((response) => {
          console.log(response.body);
          expect(response.body).to.be.an('object');
          expect(response.body).to.not.includes.keys('password');
          expect(response.body).to.have.own.property('hasDetails');
          expect(response.body.hasDetails).to.be.an('object');
          expect(response.body.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#Delete', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('should delete a user', () => {
      const payload = require('../json/user.get')(testContext);

      return wsops
        .delete(`/user/${payload.uid}?testSession=user@test.fr`)
        .then((response) => {
          expect(response.status).to.be.equal(200);
        });
    });

    // it('shouldnt delete a user', () => {
    //   const payload = require('../json/user.get')(testContext);

    //   return wsops
    //     .delete(`/user/${payload.uid}`)
    //     .then((response) => {
    //       expect(response.status).to.be.equal(200);
    //     });
    // });

    after(() => require('../teardown/user.common').run(testContext));
  });
});
