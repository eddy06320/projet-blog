const { expect } = require('chai');
const UserServices = require('../../../../src/api/user/user.services');

const UserModel = require('../../../../engine/datadealer/User');
const UserDetailsModel = require('../../../../engine/datadealer/UserDetails');
const UserRoleModel = require('../../../../engine/datadealer/UserRole');

const UserModelKeys = UserModel.tableColumns;
const UserDetailsModelKeys = UserDetailsModel.tableColumns;
const UserRoleModelKeys = UserRoleModel.tableColumns;

describe('[Services] user', () => {
  describe('#Get', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));
    it('should return undefined if !user', () => {
      const payload = require('../json/user.get')(testContext);
      payload.uid = 0;

      return UserServices.get({ payload, httpQuery: {} })
        .then((user) => {
          expect(user).to.be.equal(undefined);
        });
    });

    it('should return a user without httpQuery: {}', () => {
      const payload = require('../json/user.get')(testContext);

      return UserServices.get({ payload, httpQuery: {} })
        .then((user) => {
          console.log(user);
          expect(user).to.be.an('object');
          expect(user).to.not.includes.keys('password');
          expect(user).to.not.have.own.property('hasRole');
          expect(user).to.not.have.own.property('hasDetails');
        });
    });

    it('should return a user with httpQuery: { withPassword }', () => {
      const payload = require('../json/user.get')(testContext);

      return UserServices.get({ payload, httpQuery: { withPassword: true } })
        .then((user) => {
          console.log(user);
          expect(user).to.be.an('object');
          expect(user).to.includes.all.keys(UserModelKeys);
          expect(user).to.not.have.own.property('hasRole');
          expect(user).to.not.have.own.property('hasDetails');
        });
    });

    it('should return a user with proper dataset with httpQuery: { hasDetails }', () => {
      const payload = require('../json/user.get')(testContext);
      return UserServices.get({ payload, httpQuery: { hasDetails: true } })
        .then((user) => {
          console.log(user);
          expect(user).to.be.an('object');
          expect(user).to.not.includes.keys('password');
          expect(user).to.not.have.own.property('hasRole');
          expect(user).to.have.own.property('hasDetails');
          expect(user.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        });
    });

    it('should return a user with proper dataset with httpQuery: { hasRole }', () => {
      const payload = require('../json/user.get')(testContext);
      return UserServices.get({ payload, httpQuery: { hasRole: true } })
        .then((user) => {
          console.log(user);
          expect(user).to.be.an('object');
          expect(user).to.not.includes.keys('password');
          expect(user).to.have.own.property('hasRole');
          expect(user).to.not.have.own.property('hasDetails');
          expect(user.hasRole).to.includes.all.keys(UserRoleModelKeys);
        });
    });

    it('should return a user with proper dataset with httpQuery: { hasRole & hasDetails }', () => {
      const payload = require('../json/user.get')(testContext);
      return UserServices.get({ payload, httpQuery: { hasRole: true, hasDetails: true } })
        .then((user) => {
          console.log(user);
          expect(user).to.be.an('object');
          expect(user).to.not.includes.keys('password');
          expect(user).to.have.own.property('hasRole');
          expect(user).to.have.own.property('hasDetails');
          expect(user.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
          expect(user.hasRole).to.includes.all.keys(UserRoleModelKeys);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#GetByEmail', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('should return undefined, !user', () => {
      const payload = require('../json/user.getEmail')(testContext);
      payload.email = 'wrongMail@test.fr';

      return UserServices.getByEmail({ payload, httpQuery: {} })
        .then((user) => {
          expect(user).to.be.equal(undefined);
        });
    });

    it('should return a user fetched by email, without httpQuery: {}', () => {
      const payload = require('../json/user.getEmail')(testContext);

      return UserServices.getByEmail({ payload, httpQuery: {} })
        .then((user) => {
          console.log(user);
          expect(user).to.be.an('object');
          expect(user).to.not.includes.keys('password');
          expect(user).to.haveOwnProperty('hasDetails');
          expect(user).to.haveOwnProperty('hasRole');
        });
    });

    it('should return a user fetched by email, witht httpQuery: { withPassword }', () => {
      const payload = require('../json/user.getEmail')(testContext);

      return UserServices.getByEmail({ payload, httpQuery: { withPassword: true } })
        .then((user) => {
          console.log(user);
          expect(user).to.be.an('object');
          expect(user).to.includes.all.keys(UserModelKeys);
          expect(user).to.haveOwnProperty('hasDetails');
          expect(user).to.haveOwnProperty('hasRole');
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#Exists', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('should return true, user exists', () => {
      const payload = require('../json/user.getEmail')(testContext);

      return UserServices.exists({ payload })
        .then((res) => {
          console.log(res);
          expect(res).to.be.equal(true);
        });
    });

    it('should return false, user !exists', () => {
      const payload = require('../json/user.getEmail')(testContext);
      payload.email = 'notExist@test.fr';

      return UserServices.exists({ payload })
        .then((res) => {
          console.log(res);
          expect(res).to.be.equal(false);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#Report', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('should return a user\'s array', () => UserServices.report({ httpQuery: {} })
      .then((users) => {
        console.log(users);
        expect(users).to.be.an('array');
        expect(users[0]).to.be.an('object');
        expect(users[0]).to.not.includes.keys('password');
        expect(users[0]).to.not.have.own.property('hasRole');
        expect(users[0]).to.not.have.own.property('hasDetails');
      }));

    it('should return a user\'s array with httpQuery: { hasRole }', () => UserServices.report({ httpQuery: { hasRole: true } })
      .then((users) => {
        console.log(users);
        expect(users).to.be.an('array');
        expect(users[0]).to.be.an('object');
        expect(users[0]).to.not.includes.keys('password');
        expect(users[0]).to.not.have.own.property('hasDetails');
        expect(users[0]).to.have.own.property('hasRole');
        expect(users[0].hasRole).to.be.an('object');
        expect(users[0].hasRole).to.includes.all.keys(UserRoleModelKeys);
      }));

    it('should return a user\'s array with httpQuery: { hasDetails }', () => UserServices.report({ httpQuery: { hasDetails: true } })
      .then((users) => {
        console.log(users);
        expect(users).to.be.an('array');
        expect(users[0]).to.be.an('object');
        expect(users[0]).to.not.includes.keys('password');
        expect(users[0]).to.not.have.own.property('hasRole');
        expect(users[0]).to.have.own.property('hasDetails');
        expect(users[0].hasDetails).to.be.an('object');
        expect(users[0].hasDetails).to.includes.all.keys(UserDetailsModelKeys);
      }));

    it('should return a user\'s array with httpQuery: { hasDetails & hasRole }', () => UserServices.report({ httpQuery: { hasRole: true, hasDetails: true } })
      .then((users) => {
        console.log(users);
        expect(users).to.be.an('array');
        expect(users[0]).to.be.an('object');
        expect(users[0]).to.not.includes.keys('password');
        expect(users[0]).to.have.own.property('hasDetails');
        expect(users[0]).to.have.own.property('hasRole');
        expect(users[0].hasRole).to.be.an('object');
        expect(users[0].hasRole).to.includes.all.keys(UserRoleModelKeys);
        expect(users[0].hasDetails).to.be.an('object');
        expect(users[0].hasDetails).to.includes.all.keys(UserDetailsModelKeys);
      }));

    it('should return a user\'s array with httpQuery: { orderBy }', () => UserServices.report({ httpQuery: { orderBy: 'email' } })
      .then((users) => {
        console.log(users);
        expect(users).to.be.an('array');
        expect(users[0]).to.be.an('object');
        expect(users[0]).to.not.includes.keys('password');
        expect(users[0]).to.not.have.own.property('hasDetails');
        expect(users[0]).to.not.have.own.property('hasRole');
      }));

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#Post', () => {
    const testContext = {};

    it('should create a new user and return dataset', () => {
      const payload = require('../json/user.post')(testContext);

      return UserServices.create({ payload })
        .then((user) => {
          console.log(user);
          testContext.uid = user.uid;

          expect(user).to.be.an('object');
          expect(user).to.not.includes.keys('password');
          expect(user).to.have.own.property('hasRole');
          expect(user.hasRole).to.includes.all.keys(UserRoleModelKeys);
          expect(user).to.have.own.property('hasDetails');
          expect(user.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#UPDATEDETAILS', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('should update user details', () => {
      const payload = require('../json/user.put')(testContext);

      return UserServices.updateDetails({ payload })
        .then((user) => {
          console.log(user);
          expect(user).to.be.an('object');
          expect(user).to.not.includes.keys('password');
          expect(user).to.have.own.property('hasDetails');
          expect(user.hasDetails).to.be.an('object');
          expect(user.hasDetails).to.includes.all.keys(UserDetailsModelKeys);
          expect(user.hasDetails.pseudo).to.be.equal('Upseudo');
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#Delete', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('should delete a user and his relations', () => {
      const payload = require('../json/user.get')(testContext);
      return UserServices.delete({ payload });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });
});
