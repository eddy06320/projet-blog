const { expect } = require('chai');
const UserRulers = require('../../../../src/api/user/user.rulers.js');

describe('[Rulers] user', () => {
  describe('#Get', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('shouldn t return error, without queries', () => {
      const payload = require('../json/user.get')(testContext);

      return UserRulers.Get.validate({ payload, httpQuery: {} })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('shouldn t return error, with httpQuery: { hasDetails: true }', () => {
      const payload = require('../json/user.get')(testContext);

      return UserRulers.Get.validate({ payload, httpQuery: { hasDetails: true } })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('shouldn t return error, with httpQuery: { hasRole: true }', () => {
      const payload = require('../json/user.get')(testContext);

      return UserRulers.Get.validate({ payload, httpQuery: { hasRole: true } })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('shouldn t return error, with httpQuery: { hasRole: true, hasDetails: true }', () => {
      const payload = require('../json/user.get')(testContext);

      return UserRulers.Get.validate({ payload, httpQuery: { hasRole: true, hasDetails: true } })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('should return BadRequestError, WRONG httpQuery value', () => {
      const payload = require('../json/user.get')(testContext);

      return UserRulers.Get.validate({ payload, httpQuery: { hasRole: 1 } })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return BadRequestError, WRONG httpQuery key', () => {
      const payload = require('../json/user.get')(testContext);

      return UserRulers.Get.validate({ payload, httpQuery: { chaussette: 1 } })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, MISSING data', () => {
      const payload = require('../json/user.get')(testContext);
      delete payload.uid;

      return UserRulers.Get.validate({ payload, httpQuery: {} })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          expect(e.status).to.be.equal(400);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#Report', () => {
    it('shouldn t return error, without httpQuery: {}', () => UserRulers.Report.validate({ httpQuery: {} })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldn t return error, with httpQuery: { hasDetails }', () => UserRulers.Report.validate({ httpQuery: { hasDetails: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldn t return error, with httpQuery: { hasRole }', () => UserRulers.Report.validate({ httpQuery: { hasRole: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldn t return error, with httpQuery: { hasRole&hasDetails }', () => UserRulers.Report.validate({ httpQuery: { hasRole: true, hasDetails: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldn t return error, with httpQuery: { orderBy }', () => UserRulers.Report.validate({ httpQuery: { orderBy: 'uid' } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldn t return error, with httpQuery: { count }', () => UserRulers.Report.validate({ httpQuery: { count: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldn t return error, with httpQuery: { page&pageSize }', () => UserRulers.Report.validate({ httpQuery: { page: 0, pageSize: 1 } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('should return BadRequestError, with httpQuery: { hasRole&count }', () => UserRulers.Report.validate({ httpQuery: { hasRole: true, count: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(400);
      }));

    it('should return BadRequestError, with httpQuery: { hasRole&(page&pageSize) }', () => UserRulers.Report.validate({ httpQuery: { hasRole: true, page: 0, pageSize: 1 } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(400);
      }));

    it('should return BadRequestError, with httpQuery: { count&(page&pageSize) }', () => UserRulers.Report.validate({ httpQuery: { count: true, page: 0, pageSize: 1 } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(400);
      }));

    it('should return a BadRequestError, WRONG httpQuery key', () => UserRulers.Report.validate({ httpQuery: { chaussette: true, details: true } })
      .then(() => {
        throw new Error('Success, Error was expected');
      })
      .catch((e) => {
        expect(e.status).to.be.equal(400);
      }));

    it('should return a BadRequestError, WRONG httpQuery value', () => UserRulers.Report.validate({ httpQuery: { role: 'chaussette', details: true } })
      .then(() => {
        throw new Error('Success, Error was expected');
      })
      .catch((e) => {
        expect(e.status).to.be.equal(400);
      }));
  });

  describe('#Post', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('shouldn t return error', () => {
      const payload = require('../json/user.post')(testContext);
      // give email who doesnt exist to respect isAlreadyExist() rule
      payload.email = 'email@test.fr';

      return UserRulers.Post.validate({ payload })
        .catch((e) => {
          console.log(e);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, MISSING data', () => {
      const payload = require('../json/user.post')(testContext);
      delete payload.password;

      return UserRulers.Post.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG data', () => {
      const payload = require('../json/user.post')(testContext);
      payload.email = 3;

      return UserRulers.Post.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, email already exist', () => {
      const payload = require('../json/user.post')(testContext);

      return UserRulers.Post.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          expect(e.message).to.be.equal('User already exist !');
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, pseudo already exist', () => {
      const payload = require('../json/user.post')(testContext);
      payload.hasDetails.pseudo = 'Pseudo';

      return UserRulers.Post.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          expect(e.message).to.be.equal('User already exist !');
          expect(e.status).to.be.equal(400);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });

  describe('#PUT', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('shouldn t return error', () => {
      const payload = require('../json/user.put')(testContext);

      return UserRulers.Put.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('should return a BadRequestError, MISSING data', () => {
      const payload = require('../json/user.put')(testContext);
      delete payload.uid;

      return UserRulers.Put.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG data', () => {
      const payload = require('../json/user.put')(testContext);
      payload.uid = 'chaussette';

      return UserRulers.Put.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a NotFoundError', () => {
      const payload = require('../json/user.put')(testContext);
      payload.uid = 0;

      return UserRulers.Put.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.message).to.be.equal('User not found !');
          expect(e.status).to.be.equal(404);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });
  describe('#Delete', () => {
    const testContext = {};

    before(() => require('../setup/user.common').run(testContext));

    it('shouldn t return error', () => {
      const payload = require('../json/user.get')(testContext);

      return UserRulers.Delete.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('should return a BadRequestError, MISSING data', () => {
      const payload = require('../json/user.get')(testContext);
      delete payload.uid;

      return UserRulers.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG data', () => {
      const payload = require('../json/user.get')(testContext);
      payload.uid = 'chaussette';

      return UserRulers.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a NotFoundError', () => {
      const payload = require('../json/user.get')(testContext);
      payload.uid = 0;

      return UserRulers.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.message).to.be.equal('User not found !');
          expect(e.status).to.be.equal(404);
        });
    });

    after(() => require('../teardown/user.common').run(testContext));
  });
});
