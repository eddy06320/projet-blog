const { expect } = require('chai');
const CategoryServices = require('../../../../src/api/categories/category.services');

const CategoryModel = require('../../../../engine/datadealer/Category');
const ArticleModel = require('../../../../engine/datadealer/Article');

const CategoryModelKeys = CategoryModel.tableColums;
const ArticleModelKeys = ArticleModel.tableColumns;

describe('[SERVICES] Category', () => {
  describe('#GET', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('should return a category without httpQuery: {}', () => {
      const payload = require('../json/category.get')(testContext);

      return CategoryServices.get({ payload, httpQuery: {} })
        .then((category) => {
          console.log(category);
          expect(category).to.be.an('object');
          expect(category).to.includes.all.keys(CategoryModelKeys);
          expect(category).to.not.have.own.property('hasArticle');
        });
    });

    it('should return a category with httpQuery: { hasArticle }', () => {
      const payload = require('../json/category.get')(testContext);

      return CategoryServices.get({ payload, httpQuery: { hasArticle: true } })
        .then((category) => {
          console.log(category);
          expect(category).to.be.an('object');
          expect(category).to.includes.all.keys(CategoryModelKeys);
          expect(category).to.have.own.property('hasArticle');
          expect(category.hasArticle).to.be.an('array');
          expect(category.hasArticle[0]).to.includes.all.keys(ArticleModelKeys);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#REPORT', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('should return an categories\'s array', () => CategoryServices.report({ httpQuery: {} })
      .then((categories) => {
        console.log(categories);
        expect(categories).to.be.an('array');
        expect(categories[0]).to.includes.all.keys(CategoryModelKeys);
        expect(categories[0]).to.not.have.own.property('hasArticle');
      }));

    it('should return an categories\'s array with httpQuery: { hasArticle }', () => CategoryServices.report({ httpQuery: { hasArticle: true } })
      .then((categories) => {
        console.log(categories);
        expect(categories).to.be.an('array');
        expect(categories[0]).to.includes.all.keys(CategoryModelKeys);
        expect(categories[0]).to.have.own.property('hasArticle');
        expect(categories[0].hasArticle).to.be.an('array');
        expect(categories[0].hasArticle[0]).to.includes.all.keys(ArticleModelKeys);
      }));

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#POST', () => {
    const testContext = {};

    it('should insert a category', () => {
      const payload = require('../json/category.post')(testContext);

      return CategoryServices.create({ payload })
        .then((categorie) => {
          console.log(categorie);
          testContext.cid = categorie.cid;

          expect(categorie).to.be.an('object');
          expect(categorie).to.includes.all.keys(CategoryModelKeys);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#PUT', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('should update a category', () => {
      const payload = require('../json/category.put')(testContext);

      return CategoryServices.update({ payload })
        .then((category) => {
          console.log(category);
          expect(category).to.be.an('object');
          expect(category).to.includes.all.keys(CategoryModelKeys);
          expect(category.title).to.be.equal('Modifiacation categorie');
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#DELETE', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('should delete a category', () => {
      const payload = require('../json/category.get')(testContext);

      return CategoryServices.delete({ payload })
        .then((res) => {
          console.log(res);
          expect(res).to.be.equal(1);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });
});
