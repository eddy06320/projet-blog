const bcrypt = require('bcrypt');

const userData = () => ({
  email: 'test@test.fr',
  password: bcrypt.hashSync('test', 10),
});

const userDetailsData = ({ uid }) => ({
  userUid: uid,
  lname: 'Prenom',
  fname: 'Nom',
  pseudo: 'Pseudo',
});

const userRoleData = ({ uid }) => ({
  userUid: uid,
  role: 'admin',
});

const categoryData = () => ({
  title: 'Titre de category',
});

const articleData = ({ uid }) => ({
  user_uid: uid,
  title: 'testingTitle',
  body: 'testingBody',
  created_at: '01/12/2021',
});

const categoryHasArticleData = ({ cid, aid }) => ({
  category_cid: cid,
  article_aid: aid,
});

module.exports = {
  categoryData,
  articleData,
  categoryHasArticleData,
  userData,
  userDetailsData,
  userRoleData,
};
