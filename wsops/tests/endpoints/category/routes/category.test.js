const { expect } = require('chai');

const CategoryModel = require('../../../../engine/datadealer/Category');
const ArticleModel = require('../../../../engine/datadealer/Article');

const CategoryModelKeys = CategoryModel.tableColums;
const ArticleModelKeys = ArticleModel.tableColumns;

describe('[ROUTES] category', () => {
  describe('#GET', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('should return a category without httpQuery: {}', () => {
      const payload = require('../json/category.get')(testContext);

      return wsops
        .get(`/categories/${payload.cid}`)
        .then((response) => {
          console.log(response.body);
          expect(response.status).to.equal(200);
          expect(response.body).to.includes.all.keys(CategoryModelKeys);
          expect(response.body).to.not.have.own.property('hasArticle');
        });
    });

    it('should return a category with httpQuery: { hasArticle }', () => {
      const payload = require('../json/category.get')(testContext);

      return wsops
        .get(`/categories/${payload.cid}?hasArticle=true`)
        .then((response) => {
          console.log(response.body);
          expect(response.status).to.equal(200);
          expect(response.body).to.includes.all.keys(CategoryModelKeys);
          expect(response.body).to.have.own.property('hasArticle');
          expect(response.body.hasArticle[0]).to.includes.all.keys(ArticleModelKeys);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#REPORT', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('should return a categories\'s array, without httpQuery: { hasArticle }', () => wsops
      .request('report', '/categories')
      .then((response) => {
        console.log(response.body);
        expect(response.status).to.equal(200);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.includes.all.keys(CategoryModelKeys);
        expect(response.body[0]).to.not.have.own.property('hasArticle');
      }));

    it('should return a categories\'s array, with httpQuery: { hasArticle }', () => wsops
      .request('report', '/categories?hasArticle=true')
      .then((response) => {
        console.log(response.body);
        expect(response.status).to.equal(200);
        expect(response.body).to.be.an('array');
        expect(response.body[0]).to.be.an('object');
        expect(response.body[0]).to.includes.all.keys(CategoryModelKeys);
        expect(response.body[0]).to.have.own.property('hasArticle');
        expect(response.body[0].hasArticle).to.be.an('array');
        expect(response.body[0].hasArticle[0]).to.includes.all.keys(ArticleModelKeys);
      }));

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#POST', () => {
    const testContext = {};

    it('should insert and return a category', () => {
      const payload = require('../json/category.post')(testContext);

      return wsops
        .post('/categories?testSession=user@test.fr')
        .send(payload)
        .then((response) => {
          console.log(response.body);
          testContext.cid = response.body.cid;

          expect(response.status).to.equal(201);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(CategoryModelKeys);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#PUT', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('should update and return a category', () => {
      const payload = require('../json/category.put')(testContext);

      return wsops
        .put(`/categories/${payload.cid}?testSession=user@test.fr`)
        .send(payload)
        .then((response) => {
          console.log(response.body);
          expect(response.status).to.equal(202);
          expect(response.body).to.be.an('object');
          expect(response.body).to.includes.all.keys(CategoryModelKeys);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#DELETE', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('should delete a category', () => {
      const payload = require('../json/category.get')(testContext);

      return wsops
        .delete(`/categories/${payload.cid}?testSession=user@test.fr`)
        .then((response) => {
          expect(response.status).to.be.equal(200);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });
});
