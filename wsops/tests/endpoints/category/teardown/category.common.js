const Action = require('../../../utils/Action');
const database = require('../../../../engine/knex');

module.exports = class CategoryTeardown extends Action {
  action = {
    deleteArticle () {
      const { aid } = this.context();

      if (!aid) return undefined;

      return database('article')
        .delete()
        .where({ aid });
    },

    deleteCategory () {
      const { cid } = this.context();

      return database('category')
        .delete()
        .where({ cid });
    },
    deleteCategoryHasArticle () {
      const { cid } = this.context();

      return database('category_has_article')
        .delete()
        .where('category_cid', cid);
    },
    deleteUser () {
      const { uid } = this.context();
      if (!uid) return undefined;

      return database('user')
        .delete()
        .where({ uid });
    },

    deleteUserRole () {
      const { uid } = this.context();
      if (!uid) return undefined;

      return database('userRole')
        .delete()
        .where({ userUid: uid });
    },

    deleteUserDetails () {
      const { uid } = this.context();
      if (!uid) return undefined;

      return database('userDetails')
        .delete()
        .where({ userUid: uid });
    },
  }
};
