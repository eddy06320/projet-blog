const { expect } = require('chai');

const CategoryRuler = require('../../../../src/api/categories/category.rulers');

describe('[RULERS] category', () => {
  describe('#GET', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('shouldnt return error without httpQuery: {}', () => {
      const payload = require('../json/category.get')(testContext);

      return CategoryRuler.Get.validate({ payload })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('shouldnt return error with httpQuery: { hasArticle }', () => {
      const payload = require('../json/category.get')(testContext);

      return CategoryRuler.Get.validate({ payload, httpQuery: { hasArticle: true } })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(undefined);
        });
    });

    it('should return a BadRequestError, MISSING data', () => {
      const payload = require('../json/category.get')(testContext);
      delete payload.cid;

      return CategoryRuler.Get.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    it('should return a BadRequestError, WRONG data', () => {
      const payload = require('../json/category.get')(testContext);
      payload.cid = 'hello';

      return CategoryRuler.Get.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch((e) => {
          console.log(e.message);
          expect(e.status).to.be.equal(400);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#REPORT', () => {
    it('shouldnt return error, without httpQuery: {}', () => CategoryRuler.Report.validate({ httpQuery: {} })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('shouldnt return error, with httpQuery: { hasArticle }', () => CategoryRuler.Report.validate({ httpQuery: { hasArticle: true } })
      .catch((e) => {
        console.log(e.message);
        expect(e.status).to.be.equal(undefined);
      }));

    it('should return a BadRequestError, WRONG query key', () => CategoryRuler.Report.validate({ httpQuery: { chaussette: true } })
      .then(() => {
        throw new Error('Success, error was expected');
      })
      .catch((e) => {
        expect(e.status).to.be.equal(400);
      }));

    it('should return a BadRequestError, WRONG query value', () => CategoryRuler.Report.validate({ httpQuery: { hasArticle: 'chaussette' } })
      .then(() => {
        throw new Error('Success, error was expected');
      })
      .catch((e) => {
        expect(e.status).to.be.equal(400);
      }));
  });

  describe('#POST', () => {
    const testContext = {};

    it('shouldnt return error', () => {
      const payload = require('../json/category.post')(testContext);

      return CategoryRuler.Post.validate({ payload })
        .then(() => {
          console.log('ruler post success');
        });
    });

    it('should return a BadRequestError', () => {
      const payload = require('../json/category.post')(testContext);
      delete payload.title;

      return CategoryRuler.Post.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch(({ status }) => {
          expect(status).to.be.equal(400);
        });
    });
  });

  describe('#PUT', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('shouldnt return error', () => {
      const payload = require('../json/category.put')(testContext);

      return CategoryRuler.Put.validate({ payload })
        .then(() => console.log('ruler put success'));
    });

    it('should return a BadRequestError', () => {
      const payload = require('../json/category.put')(testContext);
      delete payload.title;

      return CategoryRuler.Put.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch(({ status }) => {
          expect(status).to.be.equal(400);
        });
    });

    it('should return a NotFoundError', () => {
      const payload = require('../json/category.put')(testContext);
      payload.cid = 0;

      return CategoryRuler.Put.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch(({ message, status }) => {
          expect(message).to.be.equal('Category not found');
          expect(status).to.be.equal(404);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });

  describe('#DELETE', () => {
    const testContext = {};

    before(() => require('../setup/category.common').run(testContext));

    it('shouldnt return error', () => {
      const payload = require('../json/category.get')(testContext);

      return CategoryRuler.Delete.validate({ payload })
        .then(() => console.log('ruler delete success'));
    });

    it('should return a BadReuestError', () => {
      const payload = require('../json/category.get')(testContext);
      delete payload.cid;

      return CategoryRuler.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch(({ status }) => {
          expect(status).to.be.equal(400);
        });
    });

    it('should return a NotFoundError', () => {
      const payload = require('../json/category.get')(testContext);
      payload.cid = 0;

      return CategoryRuler.Delete.validate({ payload })
        .then(() => {
          throw new Error('Success, error was expected');
        })
        .catch(({ message, status }) => {
          expect(message).to.be.equal('Category not found');
          expect(status).to.be.equal(404);
        });
    });

    after(() => require('../teardown/category.common').run(testContext));
  });
});
