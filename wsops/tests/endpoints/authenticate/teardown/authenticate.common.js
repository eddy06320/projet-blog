const Action = require('../../../utils/Action');
const database = require('../../../../engine/knex');

module.exports = class AuthenticateTeardown extends Action {
  action = {
    deleteUser () {
      const { uid } = this.context();

      return database('user')
        .delete()
        .where({ uid });
    },

    deleteUserRole () {
      const { uid } = this.context();
      if (!uid) return undefined;

      return database('userRole')
        .delete()
        .where({ userUid: uid });
    },

    deleteUserDetails () {
      const { uid } = this.context();
      if (!uid) return undefined;

      return database('userDetails')
        .delete()
        .where({ userUid: uid });
    },
  }
};
