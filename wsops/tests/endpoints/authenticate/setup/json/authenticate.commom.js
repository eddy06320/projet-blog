const bcrypt = require('bcrypt');

const userData = () => ({
  email: 'test@test.fr',
  password: bcrypt.hashSync('test', 10),
});

const userDetailsData = ({ uid }) => ({
  userUid: uid,
  lname: 'Prenom',
  fname: 'Nom',
  pseudo: 'Pseudo',
});

const userRoleData = ({ uid }) => ({
  userUid: uid,
  role: 'admin',
});

module.exports = {
  userData,
  userDetailsData,
  userRoleData,
};
