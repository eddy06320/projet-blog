// const { expect } = require('chai');

describe('[Routes] authenticate', () => {
  describe('#Post', () => {
    const testContext = {};

    before(() => require('../setup/authenticate.common').run(testContext));
    it('should create a new user session', () => {
      const payload = require('../json/authenticate.post')(testContext);

      return wsops
        .post('/authenticate')
        .send(payload)
        .then((response) => {
          console.log(response.body);
          expect(response.error.text).to.be.equal(undefined);
          expect(response).to.have.status(200);
          expect(response.body).to.includes.all.keys(['uid', 'email', 'hasDetails', 'hasRole']);
          expect(response.body).to.not.includes.keys('password');
        });
    });
    after(() => require('../teardown/authenticate.common').run(testContext));
  });
  describe('#Get', () => {
    it('should return the user session', () => wsops
      .get('/authenticate')
      .then((response) => {
        console.log(response.body);
        expect(response.error.text).to.be.equal(undefined);
        expect(response).to.have.status(200);
        expect(response.body).to.includes.all.keys(['uid', 'email', 'hasDetails', 'hasRole']);
        expect(response.body).to.not.includes.keys('password');
      }));
    // after(() => require('../teardown/authenticate.common').run(testContext));
  });

  describe('#Delete', () => {
    // const testContext = {};

    // before(() => require('../setup/authenticate.common').run(testContext));
    it('should destroy the user session', () => wsops
      .delete('/authenticate')
      .then((response) => {
        console.log(response.body);
        expect(response.error.text).to.be.equal(undefined);
        expect(response).to.have.status(200);
      }));
    // after(() => require('../teardown/authenticate.common').run(testContext));
  });
});
