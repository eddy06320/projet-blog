const { expect } = require('chai');
const AuthenticateRuler = require('../../../../src/api/authenticate/authenticate.rulers');

describe('[Rulers] authenticate', () => {
  describe('#Post', () => {
    const testContext = {};

    before(() => require('../setup/authenticate.common').run(testContext));
    it('shouldnt return error', () => {
      const payload = require('../json/authenticate.post')(testContext);

      return AuthenticateRuler.Post.validate({ payload })
        .catch((e) => {
          expect(e.status).to.be.equal(undefined);
        });
    });
    it('should return a NotFoundError, email dont exist in DB', () => {
      const payload = require('../json/authenticate.post')(testContext);
      payload.email = 't@test.fr';

      return AuthenticateRuler.Post.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          expect(e.message).to.be.equal('Error !');
          expect(e.status).to.be.equal(404);
        });
    });
    it('should return a BadRequestError, passwords dont match', () => {
      const payload = require('../json/authenticate.post')(testContext);
      payload.password = 't';

      return AuthenticateRuler.Post.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          // console.log(e);
          expect(e.message).to.be.equal('Passwords must be equal');
          expect(e.status).to.be.equal(400);
        });
    });
    it('should return a BadRequestError, MISSING data (email)', () => {
      const payload = require('../json/authenticate.post')(testContext);
      delete payload.email;

      return AuthenticateRuler.Post.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          expect(e.status).to.be.equal(400);
        });
    });
    it('should return a BadRequestError, MISSING data (password)', () => {
      const payload = require('../json/authenticate.post')(testContext);
      delete payload.password;

      return AuthenticateRuler.Post.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          expect(e.status).to.be.equal(400);
        });
    });
    it('should return a BadRequestError, WRONG data schema (email)', () => {
      const payload = require('../json/authenticate.post')(testContext);
      payload.email = 'testtest.fr';

      return AuthenticateRuler.Post.validate({ payload })
        .then(() => {
          throw new Error('Success Error was expected');
        })
        .catch((e) => {
          expect(e.status).to.be.equal(400);
        });
    });
    after(() => require('../teardown/authenticate.common').run(testContext));
  });
});
