
let webservice;
before(async () => {
  // const Log = require('debug-level')('assistant');
  const chai = require('chai');
  chai.use(require('chai-http'));

  // Log.info('start wsops start');
  const server = require('../../server');
  webservice = chai.request.agent(server).keepOpen();

  webservice.request = (method, ...args) => {
    const request = webservice.get(...args);
    request.method = method;
    return request;
  };

  global.wsops = webservice;
  // Log.info('start wsops end');
});

after(() => {
  // Log.info('teardown start');
  webservice.close();
  // Log.info('teardown end');
});
