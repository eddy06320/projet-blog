let database;
before(async () => {
  console.log('before all');
  database = require('../../engine/knex');
  global.database = database;
});

after(() => {
  console.log('after all');
  database.destroy();
});
