const { resolve } = require('path');
const debug = require('debug')('sql');
// const setup = require('../engine/datadealer/setup');

const knex = require('knex')({
  debug: false,
  client: 'sqlite3',
  connection: {
    filename: resolve(__dirname, '../res/projectx.db'),
  },
  // pool: {
  //   afterCreate: (conn, cb) => {
  //     conn.run('PRAGMA foreign_keys = ON', cb)
  //   },
  // },
  useNullAsDefault: true,
});

knex.on('query', (queryData) => {
  debug(`${new Date().toLocaleString()} ::: (${queryData.method}) ([${queryData.bindings}]) => ${queryData.sql}`);
});

module.exports = knex;
