const myCv = {
  fullname: 'Eddy Grosjean',
  jobTitle: 'Développeur FullStack JS Junior',
  adresses: [
    { icon: 'email', value: 'grosjean.eddy@hotmail.com' },
    { icon: 'phone', value: '06.45.87.16.71' },
    { icon: 'map-marker', value: '1 escalier Bella Vista, 06320 Cap d\'Ail' },
    { icon: 'calendar', value: 'Né le 03/07/1993' },
    { icon: 'car', value: 'Permis A et B' },
  ],
  languages: [
    { value: 'Anglais', details: 'Bonnes notions', progress: 60 },
  ],
  qualities: ['Motivation', 'Ponctualité', 'Goût à se former'],
  savoirEtre: ['Autonomie', 'Capacité d\'adaptation', 'Rigueur', 'Travail d\'équipe'],
  xpTechno: [
    { value: 'JS', details: '' },
    { value: 'NodeJS', details: '' },
    { value: 'VueJS', details: '' },
    { value: 'Git', details: '' },
  ],
  xpProfessional: [
    {
      duration: 'Septembre 2020-Avril 2021', title: 'Stage Développeur FullStack JS', company: 'ManaCorp', city: 'Roquebrune-Cap-Martin', details: 'Description de la mission ...',
    },
    {
      duration: '2018-2019', title: 'Préparateur automobile', company: 'Relooking Auto', city: 'Labège', details: 'Description de la mission ...',
    },
    {
      duration: '2016-2018', title: 'Opérateur de production', company: 'Sofermi', city: 'Saint-Orens-de-Gameville', details: 'Description de la mission ...',
    },
    {
      duration: '2014-2016', title: 'Engagé volontaire à l\'Armée de Terre', company: '1er Régiment de Chasseurs Parachutistes', city: 'Pamier', details: 'Description de la mission ...',
    },
  ],
  xpStudies: [
    {
      duration: 'Octobre 2019-Février 2020', title: 'Formation Java', company: 'Chambre du Commerce et de l\'Industrie', city: 'Nice',
    },
    {
      duration: '2011-2012', title: 'BAC Sciences et Technologies de l\'Industrie', company: 'Lycée Léonard de Vinci', city: 'Melun',
    },
  ],
};

module.exports = myCv;
