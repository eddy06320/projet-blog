module.exports = class DecisionEngine {
  static getQuestion (state, questions) {
    const aswerObject = state[state.length - 1];
    if (!aswerObject) {
      return Object.values(questions).find(question => !question.condition);
    }
    return Object.values(questions)
      .filter(question => question.condition)
      .find((question) => {
        if (question.condition.where === aswerObject.key &&
        question.condition.what === aswerObject.answer) return question;
        return undefined;
      });
  }
};
