const questions = {
  quellesTechno: {
    key: 'quellesTechno',
    label: 'quelles sont les technologies que vous utilisez ?',
    solution: ['JS', 'PHP', 'Java'],
    answer: '',
  },
  frontType: {
    key: 'frontType',
    beforeMe: 'developmentType',
    label: 'quels frameworks utilisez vous ?',
    solution: ['Vue', 'React', 'Angular'],
    answer: '',
    // condition: () => questions.developmentType.answer.includes('front'),
    condition: { where: 'developmentType', what: 'front' },
  },
  developmentType: {
    key: 'developmentType',
    beforeMe: 'quellesTechno',
    label: 'vous recherchez un développeur :',
    solution: ['front', 'back', 'fullStack'],
    answer: '',
    // condition: () => questions.quellesTechno.answer.includes('JS'),
    condition: { where: 'quellesTechno', what: 'JS' },
  },
  fullStackType: {
    key: 'fullStackType',
    beforeMe: 'developmentType',
    label: 'quels frameworks utilisez vous en front ?',
    solution: ['Vue', 'React', 'Angular'],
    answer: '',
    // condition: () => questions.developmentType.answer.includes('fullStack'),
    condition: { where: 'developmentType', what: 'fullStack' },
  },
  backType: {
    key: 'backType',
    beforeMe: 'developmentType',
    label: 'quel est le niveau attendu ?',
    solution: ['junior', 'intermediaire', 'senior'],
    answer: '',
    // condition: () => questions.developmentType.answer.includes('back'),
    condition: { where: 'developmentType', what: 'back' },
    // conditionResolver () {
    //   const { where, what } = this.backType.condition;
    //   return this[where].answer.includes(what);
    // },
    // condition (where, what) { return this[where].answer.includes(what); },
  },
};

// on défini la première question avec l'absence de la clé beforeMe
const questionsArray = Object.values(questions)
  .map((question) => {
    if (!question.beforeMe) {
      question.score = 1;
      return question;
    }
    // console.log()
    return ({
      ...question,
      // conditionResolver (orderedQuestions) {
      //   const { where, what } = questions[question.key].condition;
      //   console.log('---- DANS LE RESOLVER');
      //   console.log(where);
      //   console.log(questions[where]);
      //   console.log(questions[where].answer);
      //   console.log(what);
      //   return questions[where].answer.includes(what);
      // },
    });
  });

const orderedQuestions = [];

function recurse (arrayOfQuestionToOrder) {
  // si arrayOfQuestionToOrder est vide on sort de la fonction recursive
  if (!arrayOfQuestionToOrder.length) return;
  // si toutes les questions ont été traitées les deux array ont la même length => on sort
  if (orderedQuestions.length === questionsArray.length) return;

  // si la question a la clé score on push dans orderedQuestions
  const arrayOfQuestionToRecurseFrom = arrayOfQuestionToOrder.map((question) => {
    if (question.score) {
      orderedQuestions.push(question);
      return undefined;
    }

    // on cherche dans orderedQuestions prevQuestion.key qui est égal à question.beforMe
    const findPreviousQuestion = orderedQuestions
      .find(prevQuestion => prevQuestion.key === question.beforeMe);

    // si findPreviousQuestion on doit ajouter une clé score à question
    // qui doit valoir +1 par rapport à celui de la question précédente et on push dans acc[]
    if (findPreviousQuestion) {
      question.score = findPreviousQuestion.score + 1;
      orderedQuestions.push(question);
      return undefined;
    }

    return question;
  });

  // on récupère toutes les questions non traitées et on recommence
  const recurseFrom = arrayOfQuestionToRecurseFrom.filter(question => (question || null));
  recurse(recurseFrom);
}
recurse(questionsArray);

// on défini la question suivante qui aura un score de +1 par
// rapport à la question en cours et respectera sa propre condition
function isNextQuestion (score) {
  // on cherche TOUTES les questions avec un score de previousScore +1
  const nextQuestions = orderedQuestions.filter(nextQuestion => nextQuestion.score === score + 1);

  // on cherche LA question qui répond à sa condition
  const nextQuestion = nextQuestions.find((question) => {
    console.log('||||||||||');
    console.log(question);
    const { where, what } = orderedQuestions[score].condition;
    console.log('---- DANS LE RESOLVER');
    console.log(where);
    console.log(questions[where]);
    console.log(questions[where].answer);
    console.log(what);
    return questions[score].answer.includes(what);
    // return question;
  });
  return nextQuestion;
}

// isNextQuestion(2);
console.log('Première question');
console.log(orderedQuestions[0]);
console.log('Réponse à la question 1 => "JS"');
orderedQuestions[0].answer = 'JS';
console.log('Question 2 qui répond à la condition');
console.log(isNextQuestion(1));
console.log('Réponse à la question 2 => "front"');
orderedQuestions[1].answer = 'front';
console.log('Question 3 qui répond à la condition');
console.log(isNextQuestion(2));
