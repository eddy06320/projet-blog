const world = document.querySelector('.faces');
const {
  Engine, Render, Runner, World, Bodies,
} = Matter;

let engine = Engine.create();

function init () {
  const width = window.innerWidth;
  const height = window.innerHeight;

  engine.events = {};
  World.clear(engine.world);
  Engine.clear(engine);
  engine = Engine.create();

  const render = Render.create({
    canvas: world,
    engine,
    options: {
      wireframes: false,
      background: '#dcdde1',
      width,
      height,
    },
  });

  World.add(engine.world, [
    Bodies.rectangle(width / 2, height + 50, width, 100, {
      isStatic: true,
      render: {
        fillStyle: 'green',
      },
    }),

    Bodies.rectangle(-50, height / 2, 100, height, {
      isStatic: true,
    }),
    Bodies.rectangle(width + 50, height / 2, 100, height, {
      isStatic: true,
    }),
  ]);

  function createBall () {
    const ORIGINAL_SIZE = 1000;
    const SIZE = Math.floor(Math.random() * 76) + 30;
    const ballToCreate = Bodies.circle(Math.round(Math.random() * width), -30, 29, {
      angle: Math.PI * (Math.random() * 2 - 1),
      friction: 0.001,
      frictionAir: 0.01,
      restitution: 0.8,
      render: {
        sprite: {
          texture: './snowball.png',
          xScale: SIZE / ORIGINAL_SIZE,
          yScale: SIZE / ORIGINAL_SIZE,
        },
      },
    });

    // delete ballToCreate every 6sec
    setTimeout(() => {
      World.remove(engine.world, ballToCreate);
    }, 6000);

    return ballToCreate;
  }

  Engine.run(engine);

  Render.run(render);
  const insertBall = () => {
    const ballToInsert = createBall();
    World.add(engine.world, ballToInsert);
  };
  // insert ball to world every 0.6sec
  setInterval(insertBall, 600);
}

init();

// window.addEventListener('resize', () => {
//   init();
// });
