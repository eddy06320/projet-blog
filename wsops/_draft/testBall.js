const canvasElt = document.getElementById('canvas');
const ctx = canvasElt.getContext('2d');

canvasElt.width = window.innerWidth;
canvasElt.height = window.innerHeight;
canvasElt.style.background = '#f5efd7';

const items = [
  {
    type: 'rect',
    x: 425,
    y: 325,
    vx: 3,
    vy: 3,
    gravity: 4,
    width: 160,
    height: 90,
    color: '#f9ca24',
  },
  {
    type: 'circle',
    x: 750,
    y: 150,
    vx: 1.5,
    vy: 1.5,
    r: 60,
    color: '#ff7979',
  },
  {
    type: 'circle',
    x: 650,
    y: 550,
    vx: 3,
    vy: 3,
    r: 60,
    color: '#badc58',
  },
  {
    type: 'square',
    x: 70,
    y: 75,
    vx: 3,
    vy: 3,
    gravity: 3,
    width: 170,
    height: 170,
    color: '#c7ecee',
  },
];

function drawItems () {
  // on vide le canvas
  ctx.clearRect(0, 0, canvasElt.width, canvasElt.height);

  // on dessine tous les items de notre array
  items.forEach((item) => {
    if (item.type === 'rect' || item.type === 'square') {
      ctx.beginPath();
      ctx.fillStyle = item.color;
      ctx.fillRect(item.x, item.y, item.width, item.height);
      ctx.fill();
      ctx.closePath();
    }
    if (item.type === 'circle') {
      ctx.beginPath();
      ctx.fillStyle = item.color;
      ctx.arc(item.x, item.y, item.r, 0, Math.PI * 2, true);
      ctx.fill();
      ctx.closePath();
    }
  });
}

drawItems();

function animatedItems (event) {
  const mouseX = event.offsetX;
  const mouseY = event.offsetY;

  items.forEach((item) => {
    // si l'item est un cercle
    if (item.type === 'circle') {
      // le curseur doit être sur le périmètre ou dans l'aire du cercle
      if (mouseX <= item.x + item.r && mouseX >= item.x - item.r) {
        if (mouseY <= item.y + item.r && mouseY >= item.y - item.r) {
          // empêche le cercle de sortir du canvas
          if (item.x + item.vx + item.r > canvasElt.width) item.x -= 5;
          if (item.x - item.vx - item.r < 0) item.x += 5;
          if (item.y + item.vy + item.r > canvasElt.height) item.y -= 5;
          if (item.y - item.vy - item.r < 0) item.y += 5;
          // doit faire bouger le cercle à l'opposé de la souris
          if (mouseX >= item.x) item.x -= item.vx;
          if (mouseX <= item.x) item.x += item.vx;
          if (mouseY >= item.y) item.y -= item.vy;
          if (mouseY <= item.y) item.y += item.vy;
          window.requestAnimationFrame(drawItems);
        }
      }
    }
    if (item.type === 'rect' || item.type === 'square') {
      // le curseur doit être sur le périmètre ou dans l'aire du cercle
      if (mouseX >= item.x && mouseX <= item.x + item.width) {
        if (mouseY <= item.y + item.height && mouseY >= item.y) {
          // doit faire bouger le rectangle à l'opposé de la souris
          // moitié gauche du rectangle
          if (mouseX <= item.x + item.width / 2) item.x += item.vx;
          // moitié droite
          if (mouseX >= item.x + item.width - item.width / 2) item.x -= item.vx;
          // moitié haute
          if (mouseY <= item.y + item.height / 2) item.y += item.vy;
          // moitié basse
          if (mouseY >= item.y + item.height - item.height / 2) item.y -= item.vy;
          // empêche de sortir du canvas
          if (item.x + item.vx + item.width > canvasElt.width) item.x -= 5;
          if (item.x - item.vx < 0) item.x += 5;
          if (item.y + item.vy + item.height > canvasElt.height) item.y -= 5;
          if (item.y - item.vy < 0) item.y += 5;
          window.requestAnimationFrame(drawItems);
        }
      }
    }
  });
}
// event pour bouger les items avec la souris
canvasElt.addEventListener('mousemove', (e) => {
  animatedItems(e);
});

// ajouter de la gravité
function addGravity () {
  items.forEach((item) => {
    if (item.type === 'rect' || item.type === 'square') {
      if (item.y + item.height <= canvasElt.height) {
        item.y += item.gravity;
        window.requestAnimationFrame(drawItems);
      }
    }
  });
}
window.setInterval(addGravity, 25);

// function collisionDetected () {
//   // si le perimetre d'un item touche le perimetre d'un autre
// }
