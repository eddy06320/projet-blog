const bcrypt = require('bcrypt');

const User = require('../../../engine/datadealer/User');
const hasProperty = require('../../../engine/utils/hasProperty');

module.exports = class UserServices {
  static get ({ payload: { uid }, httpQuery }) {
    const query = User.query()
      .findById(uid);

    if (httpQuery) {
      if (hasProperty(httpQuery, 'hasDetails')) query.withGraphJoined('hasDetails');
      if (hasProperty(httpQuery, 'hasRole')) query.withGraphJoined('hasRole');
    }

    return query
      .then((user) => {
        if (!user) return undefined;
        if (httpQuery && hasProperty(httpQuery, 'withPassword')) return user;
        const { password, ...restUser } = user;
        const myUser = restUser;

        return myUser;
      });
  }

  static getByEmail ({ payload: { email }, httpQuery }) {
    const query = User.query()
      .where({ email })
      .withGraphJoined('[hasDetails, hasRole]');

    return query
      .then(([user]) => {
        if (!user) return undefined;
        if (httpQuery && hasProperty(httpQuery, 'withPassword')) return user;
        const { password, ...restUser } = user;
        const myUser = restUser;

        return myUser;
      });
  }

  static exists ({ payload: { email } }) {
    const query = User.query()
      .where({ email });

    return query
      .then(([exists]) => !!exists);
  }

  static report ({ httpQuery }) {
    const query = User.query();

    if (httpQuery) {
      if (hasProperty(httpQuery, 'hasDetails')) query.withGraphJoined('hasDetails');
      if (hasProperty(httpQuery, 'hasRole')) query.withGraphJoined('hasRole');
      if (hasProperty(httpQuery, 'orderBy')) query.orderBy(httpQuery.orderBy);
      // must not be linked with another httpQuery
      if (hasProperty(httpQuery, 'count')) return query.resultSize(); // .count('uid', { as: 'count' });
      // must not be linked with another httpQuery
      if (hasProperty(httpQuery, 'page') && hasProperty(httpQuery, 'pageSize')) {
        return query.page(httpQuery.page, httpQuery.pageSize);
      }
    }

    return query
      .then((users) => {
        const myUsers = [...users];
        myUsers.map((user) => {
          // const { password, ...restUser } = user;
          // const myUser = restUser;
          delete user.password;
          return user;
        });
        return myUsers;
      });
  }

  static create ({ payload }) {
    return bcrypt.hash(payload.password, 10)
      .then((hash) => {
        payload.password = hash;
        const query = User.query()
          .insertGraphAndFetch(payload);

        return query
          .then((user) => {
            const { password, ...restUser } = user;
            const myUser = restUser;

            return myUser;
          });
      });
  }

  static updateDetails ({ payload }) {
    const query = User.query()
      .upsertGraphAndFetch(payload);

    return query
      .then((user) => {
        const { password, ...restUser } = user;
        const myUser = restUser;

        return myUser;
      });
  }

  static delete ({ payload: { uid } }) {
    const query = User.query()
      .deleteById(uid);

    return query;
  }
};
