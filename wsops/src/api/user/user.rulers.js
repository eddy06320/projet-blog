const Ruler = require('../../../engine/services/Ruler');
const UserServices = require('./user.services');
const UserDetailsServices = require('../userDetails/userDetails.services');

const {
  UidSc,
  UserScRequiredRoleAndDetails,
  UserUpdateSc,
  QueriesAlt,
  RestQueriesSc,
} = require('../../../schemas/user');

const {
  NotFoundError,
  BadRequestError,
} = require('../../../engine/utils/customsErrors');

class Get extends Ruler {
  payloadSchema = UidSc;

  querySchema = RestQueriesSc;
}

class Report extends Ruler {
  querySchema = QueriesAlt;
}

class Post extends Ruler {
  payloadSchema = UserScRequiredRoleAndDetails;

  rules = {
    isAlreadyExist () {
      const { payload: { email } } = this.context();

      return UserServices.exists({ payload: { email } })
        .then((res) => {
          if (res) throw new BadRequestError('User already exist !');
          else {
            const { payload: { hasDetails: { pseudo } } } = this.context();

            return UserDetailsServices.getByPseudo({ payload: { pseudo } })
              .then((userPseudo) => {
                if (userPseudo) throw new BadRequestError('Pseudo already exist');
              });
          }
        });
    },
  };
}

class Put extends Ruler {
  payloadSchema = UserUpdateSc;

  rules = {
    isExist () {
      const { payload: { uid } } = this.context();

      return UserServices.get({ payload: { uid }, httpQuery: {} })
        .then((user) => {
          if (!user) throw new NotFoundError('User not found !');
        });
    },
    // isDeepEqual() need to be implemented
  }
}

class Delete extends Ruler {
  payloadSchema = UidSc;

  rules = {
    isExist () {
      const { payload: { uid } } = this.context();

      return UserServices.get({ payload: { uid }, httpQuery: {} })
        .then((user) => {
          if (!user) throw new NotFoundError('User not found !');
        });
    },
  }
}

module.exports = {
  Get,
  Report,
  Post,
  Put,
  Delete,
};
