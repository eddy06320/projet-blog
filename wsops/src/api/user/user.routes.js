const express = require('express');

const router = express();

const contextMW = require('../../../engine/middlewares/contextMW');
const righterMW = require('../../../engine/middlewares/righterMW');

const UserServices = require('./user.services');
const UserRulers = require('./user.rulers');

router.get('/:uid', righterMW('admin'), contextMW({}), ({ context }, res, next) => {
  res.log();

  UserRulers.Get.validate(context)
    .then(() => UserServices.get(context))
    .then((user) => {
      if (!user) res.status(404).json('Utilisateur introuvable');
      else {
        res.status(200).json(user);
      }
    })
    .catch(next);
});

router.report('/', contextMW({}), ({ context, session }, res, next) => {
  res.log();

  UserRulers.Report.validate(context)
    .then(() => UserServices.report(context))
    .then((users) => {
      if (!users) res.status(404).json('Pas d\'utilisateurs pour le moment');
      res.status(200).json(users);
    })
    .catch(next);
});

router.post('/', contextMW({}), ({ context }, res, next) => {
  res.log();

  UserRulers.Post.validate(context)
    .then(() => UserServices.create(context))
    .then((user) => {
      res.status(200).json(user);
    })
    .catch(next);
});

router.put('/:uid', righterMW('admin', 'user'), contextMW({}), ({ context }, res, next) => {
  res.log();

  UserRulers.Put.validate(context)
    .then(() => UserServices.updateDetails(context))
    .then(user => res.status(202).json(user))
    .catch(next);
});

router.delete('/:uid', righterMW('admin'), contextMW({}), ({ context }, res, next) => {
  res.log();

  UserRulers.Delete.validate(context)
    .then(() => UserServices.delete(context))
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = router;
