const express = require('express');

const articlesRouter = require('./article/article.routes');
const commentsRouter = require('./comments/comments.routes');
const categoriesRouter = require('./categories/category.routes');
const usersRouter = require('./user/user.routes');
const contactRouter = require('./contact/contact.routes');
const authenticateRouter = require('./authenticate/authenticate.routes');
// const vueMW = require('../../engine/middlewares/vueMW');

const api = express.Router();

api.use('/comments', commentsRouter);
api.use('/articles', articlesRouter);
api.use('/categories', categoriesRouter);
api.use('/user', usersRouter);
api.use('/contact', contactRouter);
api.use('/authenticate', authenticateRouter);

const router = express.Router();
router.use('/api', api);
// router.use('/', vueMW('dist'));

module.exports = router;
