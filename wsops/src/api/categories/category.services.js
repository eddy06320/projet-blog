const Category = require('../../../engine/datadealer/Category');
const hasProperty = require('../../../engine/utils/hasProperty');

module.exports = class CategoryServices {
  static get ({ payload: { cid }, httpQuery }) {
    const query = Category.query()
      .findById(cid);

    if (httpQuery) {
      if (hasProperty(httpQuery, 'hasArticle')) query.withGraphJoined('hasArticle');
    }

    return query;
  }

  static report ({ httpQuery }) {
    const query = Category.query();

    if (httpQuery) {
      if (hasProperty(httpQuery, 'hasArticle')) query.withGraphJoined('hasArticle');
    }

    return query;
  }

  static create ({ payload }) {
    const query = Category.query()
      .insertAndFetch(payload);

    return query;
  }

  static update ({ payload: { cid, ...category } }) {
    const query = Category.query()
      .updateAndFetchById(cid, category);

    return query;
  }

  static delete ({ payload: { cid } }) {
    const query = Category.query()
      .deleteById(cid);

    return query;
  }
};
