const Ruler = require('../../../engine/services/Ruler');
const CategoryServices = require('./category.services');

const {
  CategorySc,
  CidSc,
  CategoryScRequiredcidSc,
  HasArticleQuerySc,
} = require('../../../schemas/category');

const {
  NotFoundError,
  // BadRequestError,
} = require('../../../engine/utils/customsErrors');

class Get extends Ruler {
  payloadSchema = CidSc;

  querySchema = HasArticleQuerySc;
}

class Report extends Ruler {
  querySchema = HasArticleQuerySc;
}

class Post extends Ruler {
  payloadSchema = CategorySc;
}

class Put extends Ruler {
  payloadSchema = CategoryScRequiredcidSc;

  rules = {
    categoryExists () {
      const { payload: { cid } } = this.context();

      return CategoryServices.get({ payload: { cid } })
        .then((category) => {
          if (!category) throw new NotFoundError('Category not found');
        });
    },
  };
}

class Delete extends Ruler {
  payloadSchema = CidSc;

  rules = {
    categoryExists () {
      const { payload: { cid } } = this.context();

      return CategoryServices.get({ payload: { cid } })
        .then((category) => {
          if (!category) throw new NotFoundError('Category not found');
        });
    },
  };
}

module.exports = {
  Get,
  Report,
  Post,
  Put,
  Delete,
};
