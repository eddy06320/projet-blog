const express = require('express');

const router = express();

const CategoryServices = require('./category.services');
const CategoryRulers = require('./category.rulers');
const contextMW = require('../../../engine/middlewares/contextMW');
const righterMW = require('../../../engine/middlewares/righterMW');

router.get('/:cid', contextMW({}), ({ context }, res, next) => {
  res.log();

  CategoryRulers.Get.validate(context)
    .then(() => CategoryServices.get(context))
    .then((category) => {
      if (!category) res.status(404).json('Pas de categorie');
      else res.status(200).json(category);
    })
    .catch(next);
});

router.report('/', contextMW({}), ({ context }, res, next) => {
  res.log();

  CategoryServices.report(context)
    .then((categories) => {
      if (!categories) res.status(404).json('Pas de categories');
      else res.status(200).json(categories);
    })
    .catch(next);
});

router.post('/', righterMW('admin'), contextMW({}), ({ context }, res, next) => {
  res.log();

  CategoryRulers.Post.validate(context)
    .then(() => CategoryServices.create(context))
    .then((category) => {
      res.status(201).json(category);
    })
    .catch(next);
});

router.put('/:cid', righterMW('admin'), contextMW({}), ({ context }, res, next) => {
  res.log();

  CategoryRulers.Put.validate(context)
    .then(() => CategoryServices.update(context))
    .then(category => res.status(202).json(category))
    .catch(next);
});

router.delete('/:cid', righterMW('admin'), contextMW({}), ({ context }, res, next) => {
  res.log();

  CategoryRulers.Delete.validate(context)
    .then(() => CategoryServices.delete(context))
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = router;
