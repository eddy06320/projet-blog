const Ruler = require('../../../engine/services/Ruler');
const { MailSc } = require('../../../schemas/contact');

class Post extends Ruler {
  payloadSchema = MailSc;
}

module.exports = {
  Post,
};
