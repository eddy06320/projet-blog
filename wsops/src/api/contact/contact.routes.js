const express = require('express');

const router = express();
const ContactServices = require('./contact.services');
const ContactRulers = require('./contact.rulers');
const contextMW = require('../../../engine/middlewares/contextMW');

router.post('/', contextMW({}), ({ context }, res, next) => {
  res.log();

  ContactRulers.Post.validate(context)
    .then(() => ContactServices.postMail(context))
    .then((validate) => {
      if (validate) {
        return res.status(200).json({
          message: 'Success',
        });
      }

      return res.status(500).json({
        message: 'Failed',
      });
    })
    .catch(next);
});

module.exports = router;
