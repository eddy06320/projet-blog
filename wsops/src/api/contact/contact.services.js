const nodemailer = require('nodemailer');

module.exports = class ContactServices {
  static postMail ({ payload }) {
    const transporter = nodemailer.createTransport({
      host: 'smtp-mail.outlook.com',
      port: 587,
      secureConnection: false,
      tls: {
        ciphers: 'SSLv3',
      },
      auth: {
        user: 'grosjean.eddy.test@hotmail.com',
        pass: 'get770631',
      },
    });

    const message = {
      from: 'grosjean.eddy.test@hotmail.com',
      replyTo: payload.email,
      to: 'grosjean.eddy.test@hotmail.com',
      subject: payload.subject,
      html: `<p>${payload.message}</p><br>
        <p>Sent by: ${payload.name} | ${payload.email}</p>`,
    };

    let validate = true;

    transporter.sendMail(message, (error, info) => {
      if (error) {
        validate = false;
        return console.log(error);
      }
      console.log(`Message sent: ${info.response}`);
      return undefined;
    });
    return validate;
  }
};
