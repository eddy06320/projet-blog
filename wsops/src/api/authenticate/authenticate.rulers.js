const bcrypt = require('bcrypt');

const Ruler = require('../../../engine/services/Ruler');
const UserServices = require('../user/user.services');

const { NotFoundError, BadRequestError } = require('../../../engine/utils/customsErrors');
const { AuthenticateSc } = require('../../../schemas/authenticate');

class Post extends Ruler {
  payloadSchema = AuthenticateSc;

  rules = {
    // Le user doit exister en base
    isExist () {
      const { payload: { email } } = this.context();

      return UserServices.exists({ payload: { email } })
        .then((res) => {
          if (!res) throw new NotFoundError('Error !');
        });
    },
    // Le combo email && password doit être correct
    passwordConfirmation () {
      const { payload } = this.context();

      // On récupère le user avec son pass en DB
      // eslint-disable-next-line max-len
      return UserServices.getByEmail({ payload: { email: payload.email }, httpQuery: { withPassword: true } })
        .then(user => bcrypt.compare(payload.password, user.password)
          .then((result) => {
            if (!result) throw new BadRequestError('Passwords must be equal');
          }));
    },
  }
}

module.exports = {
  Post,
};
