const express = require('express');

const router = express();
const ContextMW = require('../../../engine/middlewares/contextMW');

const AuthenticateRulers = require('./authenticate.rulers');
const UserServices = require('../user/user.services');

router.get('/', ContextMW({}), ({ session }, res, next) => {
  res.log();

  res.status(200).send(session.user ? session.user : {});

  return next();
});

router.post('/', ContextMW({}), ({ context, session }, res, next) => {
  res.log();

  AuthenticateRulers.Post.validate(context)
    .then(() => UserServices.getByEmail(context))
    .then((user) => {
      context.user = user;
      session.user = user;
    })
    .then(() => {
      res.status(200).send(context.user);

      return next();
    })
    .catch(next);
});

router.delete('/', ContextMW({}), ({ session }, res, next) => {
  res.log();

  session.destroy();
  res.sendStatus(200);

  next();
});

module.exports = router;
