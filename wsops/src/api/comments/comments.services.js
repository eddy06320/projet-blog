const Comment = require('../../../engine/datadealer/Comment');
const hasProperty = require('../../../engine/utils/hasProperty');

module.exports = class CommentsServices {
  static get ({ payload: { cid }, httpQuery }) {
    const query = Comment.query()
      .findById(cid);

    if (httpQuery) {
      if (hasProperty(httpQuery, 'hasArticle')) query.withGraphJoined('hasArticle');
      if (hasProperty(httpQuery, 'hasUser')) query.withGraphJoined('hasUser.[hasDetails, hasRole]');
    }

    return query
      .then((comment) => {
        if (hasProperty(httpQuery, 'hasUser')) {
          const myComment = { ...comment };
          const { password, ...restUser } = comment.hasUser;
          myComment.hasUser = restUser;

          return myComment;
        }
        return comment;
      });
  }

  static report ({ httpQuery }) {
    const query = Comment.query();

    if (httpQuery) {
      if (hasProperty(httpQuery, 'hasArticle')) query.withGraphJoined('hasArticle');
      if (hasProperty(httpQuery, 'hasUser')) query.withGraphJoined('hasUser.[hasDetails, hasRole]');
      // must not be linked with another httpQuery
      if (hasProperty(httpQuery, 'count')) return query.resultSize(); // .count('cid', { as: 'count' });
    }

    return query
      .then((comments) => {
        if (hasProperty(httpQuery, 'hasUser')) {
          const myComments = [...comments];
          myComments.map((comment) => {
            const { hasUser: { password, ...restUser } } = comment;
            comment.hasUser = restUser;

            return comment;
          });
          return myComments;
        }
        return comments;
      });
  }

  static create ({ payload }) {
    const query = Comment.query()
      .insertGraphAndFetch(payload, {
        relate: ['hasArticle'],
      });

    return query;
  }

  static update ({ payload: { cid, ...comment } }) {
    const query = Comment.query()
      .updateAndFetchById(cid, comment);

    return query;
  }

  static delete ({ payload: { cid } }) {
    const query = Comment.query()
      .deleteById(cid);

    return query;
  }
};
