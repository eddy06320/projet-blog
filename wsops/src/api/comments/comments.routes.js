const express = require('express');

const router = express();
const CommentsServices = require('./comments.services');
const CommentRulers = require('./comments.rulers');
const contextMW = require('../../../engine/middlewares/contextMW');
const righterMW = require('../../../engine/middlewares/righterMW');

router.get('/:cid', contextMW({}), ({ context }, res, next) => {
  res.log();

  CommentRulers.Get.validate(context)
    .then(() => CommentsServices.get(context))
    .then((comment) => {
      if (!comment) res.status(404).json('Comment not found');
      else res.status(200).json(comment);
    })
    .catch(next);
});

router.report('/', contextMW({}), ({ context }, res, next) => {
  res.log();

  CommentsServices.report(context)
    .then((comments) => {
      if (!comments) res.status(404).json('No comments found');
      else res.status(200).json(comments);
    })
    .catch(next);
});

router.post('/', righterMW('admin', 'user'), contextMW({}), ({ context, session }, res, next) => {
  res.log();

  CommentRulers.Post.validate(context)
    .then(() => CommentsServices.create(context))
    .then(comment => res.status(201).json(comment))
    .catch(next);
});

router.put('/:cid', righterMW('admin', 'user'), contextMW({}), ({ context }, res, next) => {
  res.log();

  CommentRulers.Put.validate(context)
    .then(() => CommentsServices.update(context))
    .then(comment => res.status(202).json(comment))
    .catch(next);
});

router.delete('/:cid', righterMW('admin', 'user'), contextMW({}), ({ context }, res, next) => {
  res.log();

  CommentRulers.Delete.validate(context)
    .then(() => CommentsServices.delete(context))
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = router;
