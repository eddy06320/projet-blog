const Ruler = require('../../../engine/services/Ruler');
const CommentsServices = require('./comments.services');

const {
  CommentSc,
  CidSc,
  CommentScRequiredCidSc,
  QueriesAlt,
  RestQueriesSc,
} = require('../../../schemas/comment');

const {
  NotFoundError,
  // BadRequestError,
} = require('../../../engine/utils/customsErrors');

class Get extends Ruler {
  payloadSchema = CidSc;

  querySchema = RestQueriesSc;
}

class Report extends Ruler {
  querySchema = QueriesAlt;
}

class Post extends Ruler {
  payloadSchema = CommentSc;
}

class Put extends Ruler {
  payloadSchema = CommentScRequiredCidSc;

  rules = {
    articleExists () {
      const { payload: { cid } } = this.context();

      return CommentsServices.get({ payload: { cid }, httpQuery: {} })
        .then((comment) => {
          if (!comment) throw new NotFoundError('Comment not found');
        });
    },
  }
}

class Delete extends Ruler {
  payloadSchema = CidSc;

  rules = {
    articleExists () {
      const { payload: { cid } } = this.context();

      return CommentsServices.get({ payload: { cid }, httpQuery: {} })
        .then((comment) => {
          if (!comment) throw new NotFoundError('Comment not found');
        });
    },
  }
}

module.exports = {
  Get,
  Report,
  Post,
  Put,
  Delete,
};
