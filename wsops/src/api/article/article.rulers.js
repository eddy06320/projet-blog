const Ruler = require('../../../engine/services/Ruler');
const ArticleServices = require('./article.services');

const {
  ArticleSc, AidSc, ArticleUpdateSc, RestQueriesSc, QueriesAlt,
} = require('../../../schemas/article');

const {
  NotFoundError,
  // BadRequestError,
} = require('../../../engine/utils/customsErrors');

class Get extends Ruler {
  payloadSchema = AidSc;

  querySchema = RestQueriesSc;
}

class Report extends Ruler {
  querySchema = QueriesAlt;
}
class Post extends Ruler {
  payloadSchema = ArticleSc;
}

class Put extends Ruler {
  payloadSchema = ArticleUpdateSc;

  rules = {
    articleExists () {
      const { payload: { aid } } = this.context();

      return ArticleServices.get({ payload: { aid }, httpQuery: {} })
        .then((article) => {
          if (!article) throw new NotFoundError('Article not found bitches');
        });
    },
  };
}

class Delete extends Ruler {
  payloadSchema = AidSc;

  rules = {
    articleExists () {
      const { payload: { aid } } = this.context();

      return ArticleServices.get({ payload: { aid }, httpQuery: {} })
        .then((article) => {
          if (!article) throw new NotFoundError('Article not found bitches');
        });
    },
  };
}

module.exports = {
  Post,
  Delete,
  Get,
  Put,
  Report,
};
