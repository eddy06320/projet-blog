const Article = require('../../../engine/datadealer/Article');
const hasProperty = require('../../../engine/utils/hasProperty');

module.exports = class ArticlesServices {
  static get ({ payload: { aid }, httpQuery }) {
    const query = Article.query()
      .findById(aid);

    if (httpQuery) {
      if (hasProperty(httpQuery, 'hasComment')) query.withGraphJoined('hasComment.userDetail');
      if (hasProperty(httpQuery, 'hasCategory')) query.withGraphJoined('hasCategory');
      if (hasProperty(httpQuery, 'hasUser')) query.withGraphJoined('hasUser.[hasDetails, hasRole]');
    }

    return query
      .then((article) => {
        if (hasProperty(httpQuery, 'hasUser')) {
          const newArticle = { ...article };
          delete newArticle.hasUser.password;

          return newArticle;
        }
        return article;
      });
  }

  static report ({ httpQuery }) {
    const query = Article.query();

    if (httpQuery) {
      if (hasProperty(httpQuery, 'hasComment')) query.withGraphFetched('hasComment.userDetail'); // .userDetail
      if (hasProperty(httpQuery, 'hasCategory')) query.withGraphJoined('hasCategory');
      if (hasProperty(httpQuery, 'hasUser')) query.withGraphJoined('hasUser.[hasDetails, hasRole]');
      if (hasProperty(httpQuery, 'limit')) query.limit(httpQuery.limit);
      if (hasProperty(httpQuery, 'orderByDesc')) query.orderBy(httpQuery.orderByDesc, 'desc');
      if (hasProperty(httpQuery, 'orderByComment')) {
        query.select(
          'Article.*',
          Article.relatedQuery('hasComment')
            .count()
            .as('numberOfComments')
        ).orderBy('numberOfComments', 'desc');
      }
      // must not be linked with another httpQuery and return here
      if (hasProperty(httpQuery, 'count')) return query.resultSize(); // .count('aid', { as: 'count' });
      // must not be linked with another httpQuery and return here
      if (hasProperty(httpQuery, 'page') && hasProperty(httpQuery, 'pageSize')) {
        return query.page(httpQuery.page, httpQuery.pageSize);
      }
    }

    return query
      .then((articles) => {
        if (hasProperty(httpQuery, 'hasUser')) {
          const myArticles = [...articles];
          myArticles.map((article) => {
            const { hasUser: { password, ...restUser } } = article;
            article.hasUser = restUser;
            return article;
          });
          return myArticles;
        }
        return articles;
      });
  }

  static create ({ payload, httpQuery }) {
    const query = Article.query()
      .insertGraphAndFetch(payload, {
        relate: ['hasCategory'],
      });

    if (httpQuery) {
      if (hasProperty(httpQuery, 'withUser')) {
        query.withGraphFetched('hasUser.[hasDetails, hasRole]');
      }
    }

    return query
      .then((article) => {
        if (article.hasUser) {
          const newArticles = { ...article };
          delete newArticles.hasUser.password;

          return newArticles;
        }
        return article;
      });
  }

  static update ({ payload }) {
    // console.log(article);
    const query = Article.query()
      // .updateAndFetchById(aid, article);
      .upsertGraphAndFetch(payload, {
        relate: ['hasCategory'],
        unrelate: ['hasCategory'],
      });

    return query;
  }

  static delete ({ payload: { aid } }) {
    const query = Article.query()
      .deleteById(aid);

    return query;
  }
};
