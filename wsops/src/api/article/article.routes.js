const express = require('express');

const router = express();

const ArticleServices = require('./article.services');
const ArticleRulers = require('./article.rulers');

const contextMW = require('../../../engine/middlewares/contextMW');
const righterMW = require('../../../engine/middlewares/righterMW');

router.get('/:aid', contextMW({}), ({ context }, res, next) => {
  res.log();

  ArticleRulers.Get.validate(context)
    .then(() => ArticleServices.get(context))
    .then((article) => {
      if (!article) res.status(404).json('Pas d\'article');
      else {
        res.status(200).json(article);
      }
    })
    .catch(next);
});

router.report('/', contextMW({}), ({ context }, res, next) => {
  res.log();
  ArticleRulers.Report.validate(context)
    .then(() => ArticleServices.report(context))
    .then((articles) => {
      if (!articles) res.status(404).json('Pas d\'articles');
      else res.status(200).json(articles);
    })
    .catch(next);
});

router.post('/', righterMW('admin'), contextMW({}), ({ context }, res, next) => {
  res.log();

  ArticleRulers.Post.validate(context)
    .then(() => ArticleServices.create(context))
    .then((article) => {
      res.status(201).json(article);
    })
    .catch(next);
});

router.put('/:aid', righterMW('admin'), contextMW({}), ({ context }, res, next) => {
  res.log();

  ArticleRulers.Put.validate(context)
    .then(() => ArticleServices.update(context))
    .then(article => res.status(202).json(article))
    .catch(next);
});

router.delete('/:aid', righterMW('admin'), contextMW({}), ({ context }, res, next) => {
  res.log();

  ArticleRulers.Delete.validate(context)
    .then(() => ArticleServices.delete(context))
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = router;
