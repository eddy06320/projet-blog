const Ruler = require('../../../engine/services/Ruler');
const UserDetailsServices = require('./userDetails.services');

const { UserUidSc, UserDetailsSc } = require('../../../schemas/userDetails');
const { NotFoundError, BadRequestError } = require('../../../engine/utils/customsErrors');

class Get extends Ruler {
  payloadSchema = UserUidSc;
}

class Put extends Ruler {
  payloadSchema = UserDetailsSc;

  rules = {
    isNotExist () {
      const { payload: { userUid } } = this.context();

      return UserDetailsServices.get({ payload: { userUid } })
        .then((userDetails) => {
          if (!userDetails) throw new NotFoundError('userDetails not found');
        });
    },
    isAlreadyExist () {
      const { payload: { pseudo } } = this.context();

      return UserDetailsServices.getByPseudo({ payload: { pseudo } })
        .then((response) => {
          if (response) throw new BadRequestError('Pseudo already exist');
        });
    },
  };
}

module.exports = {
  Get,
  Put,
};
