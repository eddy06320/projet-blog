const express = require('express');

const router = express();
const contextMW = require('../../../engine/middlewares/contextMW');
const righterMW = require('../../../engine/middlewares/righterMW');
const UserDetailsServices = require('./userDetails.services');
const UserDetailsRulers = require('./userDetails.rulers');

router.get('/:userUid', contextMW({}), ({ context }, res, next) => {
  res.log();

  UserDetailsRulers.Get.validate(context)
    .then(() => UserDetailsServices.get(context))
    .then((userDetails) => {
      if (!userDetails) res.status(404).json('UserDetails not found');
      else res.status(200).json(userDetails);
    })
    .catch(next);
});

router.report('/', righterMW('admin'), contextMW({}), ({ context }, res, next) => {
  res.log();

  UserDetailsServices.report()
    .then((usersDetails) => {
      if (!usersDetails) res.status(404).json('UsersDetails not found');
      else res.status(200).json(usersDetails);
    })
    .catch(next);
});

router.put('/:userUid', righterMW('admin', 'user'), contextMW({}), ({ context }, res, next) => {
  res.log();

  UserDetailsRulers.Put.validate(context)
    .then(() => UserDetailsServices.update(context))
    .then(userDetails => res.status(200).json(userDetails))
    .catch(next);
});

module.exports = router;
