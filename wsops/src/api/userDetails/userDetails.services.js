const UserDetails = require('../../../engine/datadealer/UserDetails');

module.exports = class UserDetailsServices {
  static get ({ payload: { userUid } }) {
    const query = UserDetails.query()
      .findById(userUid);

    return query;
  }

  static getByPseudo ({ payload: { pseudo } }) {
    const query = UserDetails.query()
      .findOne({ pseudo });

    return query;
  }

  static report () {
    const query = UserDetails.query();

    return query;
  }

  static update ({ payload: { userUid, ...userDetails } }) {
    const query = UserDetails.query()
      .updateAndFetchById(userUid, userDetails);

    return query;
  }
};
