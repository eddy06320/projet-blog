const Joi = require('joi');

const EmailSc = Joi.string().email().required();
const PasswordSc = Joi.string().required();

const AuthenticateSc = Joi.object().keys({
  email: EmailSc,
  password: PasswordSc,
});

module.exports = {
  AuthenticateSc,
};
