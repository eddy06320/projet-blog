const Joi = require('joi');

const UserUidSc = Joi.object().keys({
  userUid: Joi.number().min(1).required(),
});

const UserDetailsSc = Joi.object().keys({
  userUid: Joi.number().min(1).required(),
  lname: Joi.string().min(2).max(50).required(),
  fname: Joi.string().min(2).max(50).required(),
  pseudo: Joi.string().min(2).max(50).required(),
});

module.exports = {
  UserUidSc,
  UserDetailsSc,
};
