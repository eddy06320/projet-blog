const Joi = require('joi');

const MailSc = Joi.object().keys({
  subject: Joi.string().required(),
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  message: Joi.string().required(),
});

module.exports = {
  MailSc,
};
