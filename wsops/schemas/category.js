const Joi = require('joi');

const BooleanOrEmptyString = Joi.alternatives()
  .try(Joi.boolean(), Joi.string().allow('').max(0));

const TitleSc = Joi.string()
  .min(3).max(100)
  .required();

const CidSc = Joi.object().keys({
  cid: Joi.number().min(0).required(),
});

const CategorySc = Joi.object().keys({
  title: TitleSc,
});

const CategoryScRequiredcidSc = CategorySc.concat(CidSc);

const HasArticleQuerySc = Joi.object().keys({
  hasArticle: BooleanOrEmptyString,
});

module.exports = {
  CidSc,
  CategorySc,
  CategoryScRequiredcidSc,
  HasArticleQuerySc,
};
