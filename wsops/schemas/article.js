const Joi = require('joi');
const { CommentSc } = require('./comment');
const { UserScRequiredRoleAndDetails } = require('./user');

const BooleanOrEmptyString = Joi.alternatives()
  .try(Joi.boolean(), Joi.string().allow('').max(0));

const UserUidSc = Joi.number().min(0).required();

const TitleSc = Joi.string()
  .min(5).max(100)
  .required();

const BodySc = Joi.string()
  .min(10).max(5000)
  .required();

const AidSc = Joi.object().keys({
  aid: Joi.number().min(0).required(),
});

// const HasCommentsSc = Joi.object({
//   CommentSc,
// }).optional();

const HasUserSc = Joi.object({
  UserScRequiredRoleAndDetails,
});

const HasCommentsSc = Joi.array().items(
  Joi.object({
    CommentSc,
  })
);

const HasCategorySc = Joi.array().required();

const ArticleSc = Joi.object().keys({
  hasComment: HasCommentsSc,
  user_uid: UserUidSc,
  title: TitleSc,
  body: BodySc,
  created_at: Joi.string().required(),
  hasCategory: HasCategorySc,
  hasUser: HasUserSc,
}).required();

const ArticleScRequiredAidSc = ArticleSc.concat(AidSc);

const ArticleUpdateSc = Joi.object().keys({
  aid: Joi.number().min(0).required(),
  title: TitleSc,
  body: BodySc,
  hasCategory: HasCategorySc,
  // user_uid: UserUidSc,
}).required();

const CountQuerySc = Joi.object().keys({
  count: BooleanOrEmptyString,
});

const PageAndPageSizeQuerySc = Joi.object().keys({
  page: Joi.number().min(0).optional(),
  pageSize: Joi.number().min(0).optional(),
});

const RestQueriesSc = Joi.object().keys({
  limit: Joi.number().min(0).optional(),
  orderByDesc: Joi.string().optional(),
  hasCategory: BooleanOrEmptyString,
  hasUser: BooleanOrEmptyString,
  orderByComment: BooleanOrEmptyString,
  hasComment: BooleanOrEmptyString,
});

const QueriesAlt = Joi.alternatives().try(CountQuerySc, PageAndPageSizeQuerySc, RestQueriesSc);

module.exports = {
  ArticleSc,
  AidSc,
  ArticleScRequiredAidSc,
  ArticleUpdateSc,
  QueriesAlt,
  RestQueriesSc,
};
