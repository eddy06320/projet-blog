const Joi = require('joi');

const BooleanOrEmptyString = Joi.alternatives()
  .try(Joi.boolean(), Joi.string().allow('').max(0));

const EmailSc = Joi.string().email().required();

const PasswordSc = Joi.string().min(5).max(50).required();

const UidSc = Joi.object().keys({
  uid: Joi.number().min(0).required(),
});

const RoleRequiredSc = Joi.object().keys({
  role: Joi.string().required(),
}).required();

const DetailsRequired = Joi.object().keys({
  lname: Joi.string().min(1).max(50).required(),
  fname: Joi.string().min(1).max(50).required(),
  pseudo: Joi.string().min(1).max(50).required(),
}).required();

const UserScRequiredRoleAndDetails = Joi.object().keys({
  email: EmailSc,
  password: PasswordSc,
  hasRole: RoleRequiredSc,
  hasDetails: DetailsRequired,
}).required();

const UserUpdateSc = Joi.object().keys({
  uid: Joi.number().required(),
  email: EmailSc,
  // hasRole: RoleRequiredSc,
  hasDetails: DetailsRequired,
}).required();

const UserQueriesSc = Joi.object().keys({
  role: Joi.boolean(),
  details: Joi.boolean(),
  count: Joi.boolean(),
  page: Joi.number(),
  pageSize: Joi.number(),
  orderBy: Joi.string(),
});

const CountQuerySc = Joi.object().keys({
  count: BooleanOrEmptyString,
});

const PageAndPageSizeQuerySc = Joi.object().keys({
  page: Joi.number().min(0).optional(),
  pageSize: Joi.number().min(0).optional(),
});

const RestQueriesSc = Joi.object().keys({
  orderBy: Joi.string().optional(),
  hasDetails: BooleanOrEmptyString,
  hasRole: BooleanOrEmptyString,
});

const QueriesAlt = Joi.alternatives().try(CountQuerySc, PageAndPageSizeQuerySc, RestQueriesSc);

module.exports = {
  UidSc,
  UserScRequiredRoleAndDetails,
  UserQueriesSc,
  UserUpdateSc,
  QueriesAlt,
  RestQueriesSc,
};
