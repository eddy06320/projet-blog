const Joi = require('joi');

const BooleanOrEmptyString = Joi.alternatives()
  .try(
    Joi.boolean(),
    Joi.string().allow('').max(0)
  );

const CidSc = Joi.object().keys({
  cid: Joi.number().min(0).required(),
});

const TitleSc = Joi.string()
  .min(5).max(100)
  .required();

const BodySc = Joi.string()
  .min(2).max(250)
  .required();

const CommentSc = Joi.object().keys({
  user_uid: Joi.number().min(0).required(),
  title: TitleSc,
  body: BodySc,
  created_at: Joi.string().required(),
  hasArticle: Joi.object().keys({
    aid: Joi.number().min(0).required(),
  }),
});

const CommentScRequiredCidSc = CommentSc.concat(CidSc);

const CountQuerySc = Joi.object().keys({
  count: BooleanOrEmptyString,
});

const RestQueriesSc = Joi.object().keys({
  hasArticle: BooleanOrEmptyString,
  hasUser: BooleanOrEmptyString,
});

const QueriesAlt = Joi.alternatives().try(CountQuerySc, RestQueriesSc);

module.exports = {
  CommentSc,
  CidSc,
  CommentScRequiredCidSc,
  QueriesAlt,
  RestQueriesSc,
};
