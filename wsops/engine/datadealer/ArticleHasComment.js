const { Model } = require('objection');
const path = require('path');
const knex = require('../knex');

Model.knex(knex);

module.exports = class ArticleHasComment extends Model {
  static get tableName () { return 'article_has_comment'; }

  static get idColumn () { return ['article_aid', 'comment_cid']; }

  static get relationMappings () {
    return {
      hasComment: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'Comment'),
        join: {
          from: ['article_has_comment.comment_cid'],
          to: ['comment.cid'],
        },
      },
      hasArticle: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'Article'),
        join: {
          from: ['article_has_comment.article_aid'],
          to: ['article.aid'],
        },
      },
    };
  }

  static get tableColumns () { return ['article_aid', 'comment_cid']; }
};
