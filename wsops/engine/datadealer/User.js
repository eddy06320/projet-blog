const { Model } = require('objection');
const path = require('path');
const knex = require('../knex');

Model.knex(knex);

module.exports = class User extends Model {
  static get tableName () { return 'user'; }

  static get idColumn () { return 'uid'; }

  static get relationMappings () {
    return {
      hasRole: {
        relation: Model.HasOneRelation,
        modelClass: path.join(__dirname, 'UserRole'),
        join: {
          from: 'user.uid',
          to: 'userRole.userUid',
        },
      },
      hasDetails: {
        relation: Model.HasOneRelation,
        modelClass: path.join(__dirname, 'UserDetails'),
        join: {
          from: 'user.uid',
          to: 'userDetails.userUid',
        },
      },
    };
  }

  static get tableColumns () { return ['uid', 'email', 'password']; }
};
