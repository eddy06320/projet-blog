const { Model } = require('objection');
const path = require('path');
const knex = require('../knex');

Model.knex(knex);

module.exports = class Category extends Model {
  static get tableName () { return 'category'; }

  static get idColumn () { return 'cid'; }

  static get relationMappings () {
    return {
      hasArticle: {
        relation: Model.ManyToManyRelation,
        modelClass: path.join(__dirname, 'Article'),
        join: {
          from: ['category.cid'],
          through: {
            from: 'category_has_article.category_cid',
            to: 'category_has_article.article_aid',
          },
          to: ['article.aid'],
        },
      },
    };
  }

  static get tableColums () { return ['cid', 'title']; }
};
