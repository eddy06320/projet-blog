const { Model } = require('objection');
const path = require('path');
const knex = require('../knex');

Model.knex(knex);

module.exports = class CategoryHasArticle extends Model {
  static get tableName () { return 'category_has_article'; }

  static get idColumn () { return ['category_cid', 'article_aid']; }

  static getRelationMapping () {
    return {

      hasArticles: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'Article'),
        join: {
          from: ['category_has_article.article_aid'],
          to: ['article.aid'],
        },
      },

      hasCategory: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'Category'),
        join: {
          from: ['category_has_article.category_cid'],
          to: ['category.cid'],
        },
      },
    };
  }

  static get tableColumns () { return ['category_cid', 'article_aid']; }
};
