const { Model } = require('objection');
const path = require('path');
const knex = require('../knex');

Model.knex(knex);

module.exports = class UserRole extends Model {
  static get tableName () { return 'userRole'; }

  static get idColumn () { return 'userUid'; }

  static get relationMappings () {
    return {
      roleHasUser: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'User'),
        join: {
          from: 'userRole.userUid',
          to: 'user.uid',
        },
      },
    };
  }

  static get tableColumns () { return ['userUid', 'role']; }
};
