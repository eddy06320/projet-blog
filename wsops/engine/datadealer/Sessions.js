const { Model } = require('objection');
// const path = require('path');
const knex = require('../knex');

Model.knex(knex);

module.exports = class Sessions extends Model {
  static get tableName () { return 'sessions'; }

  static get idColumn () { return 'sid'; }

  static get tableColumns () { return ['sid', 'sess', 'expired']; }
};
