const { Model } = require('objection');
const path = require('path');
const knex = require('../knex');

Model.knex(knex);

module.exports = class UserHasComment extends Model {
  static get tableName () { return 'user_has_comment'; }

  static get idColumn () { return ['user_uid', 'comment_cid']; }

  static getRelationMapping () {
    return {
      hasComment: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'Comment'),
        join: {
          from: ['user_has_comment.comment_cid'],
          to: ['comment.cid'],
        },
      },
      hasUser: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'User'),
        join: {
          from: ['user_has_comment.user_uid'],
          to: ['user.uid'],
        },
      },
    };
  }

  static get tableColumns () { return ['user_uid', 'comment_cid']; }
};
