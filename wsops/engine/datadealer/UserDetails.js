const { Model } = require('objection');
const path = require('path');
const knex = require('../knex');

Model.knex(knex);

module.exports = class UserDetails extends Model {
  static get tableName () { return 'userDetails'; }

  static get idColumn () { return 'userUid'; }

  static get relationMappings () {
    return {
      detailsHasUser: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'User'),
        join: {
          from: 'userDetails.userUid',
          to: 'user.uid',
        },
      },
    };
  }

  static get tableColumns () { return ['userUid', 'lname', 'fname', 'pseudo']; }
};
