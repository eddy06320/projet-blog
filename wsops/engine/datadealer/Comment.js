const { Model } = require('objection');
const path = require('path');
const knex = require('../knex');

Model.knex(knex);

module.exports = class Comment extends Model {
  static get tableName () { return 'comment'; }

  static get idColumn () { return 'cid'; }

  static get relationMappings () {
    return {
      hasUser: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'User'),
        join: {
          from: ['comment.user_uid'],
          to: ['user.uid'],
        },
      },
      userDetail: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'UserDetails'),
        join: {
          from: ['comment.user_uid'],
          to: ['userDetails.userUid'],
        },
      },
      hasArticle: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'Article'),
        join: {
          from: ['comment.article_aid'],
          to: ['article.aid'],
        },
      },
    };
  }

  static get tableColumns () { return ['cid', 'article_aid', 'user_uid', 'title', 'body', 'created_at']; }
};
