const { Model } = require('objection');
const path = require('path');
const knex = require('../knex');

Model.knex(knex);
module.exports = class Article extends Model {
  static get tableName () { return 'article'; }

  static get idColumn () { return 'aid'; }

  static get relationMappings () {
    return {
      hasComment: {
        relation: Model.HasManyRelation,
        modelClass: path.join(__dirname, 'Comment'),
        join: {
          from: ['article.aid'],
          to: ['comment.article_aid'],
        },
      },
      hasUser: {
        relation: Model.BelongsToOneRelation,
        modelClass: path.join(__dirname, 'User'),
        join: {
          from: ['article.user_uid'],
          to: ['user.uid'],
        },
      },
      hasCategory: {
        relation: Model.ManyToManyRelation,
        modelClass: path.join(__dirname, 'Category'),
        join: {
          from: ['article.aid'],
          through: {
            from: 'category_has_article.article_aid',
            to: 'category_has_article.category_cid',
          },
          to: ['category.cid'],
        },
      },
    };
  }

  static get tableColumns () { return ['aid', 'user_uid', 'title', 'body', 'created_at']; }
};
