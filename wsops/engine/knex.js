const { resolve } = require('path');
const debug = require('debug')('sql');
const knex = require('knex')({
  debug: false,
  client: 'sqlite3',
  connection: {
    filename: resolve(__dirname, '../res/projectx.db'),
  },
  useNullAsDefault: true,
});

knex.raw('PRAGMA foreign_keys = ON');

knex.on('query', (queryData) => {
  debug(`${new Date().toLocaleString()} ::: (${queryData.method}) ([${queryData.bindings}]) => ${queryData.sql}`);
});

module.exports = knex;
