const Joi = require('joi');

const { BadRequestError } = require('../utils/customsErrors');
// const Errors = require('../exceptions/Errors');

class Ruler {
  constructor (context) {
    this.contextData = context;
    // this.sessionData = context.session;
  }

  static validate (context) {
    const instance = new this(context);

    // eslint-disable-next-line no-use-before-define
    const evalRuler = rulerSchema.validate(instance);

    if (evalRuler.error) Promise.reject(evalRuler.error);

    let chainPromise = Promise.resolve();

    chainPromise = instance.payloadSchema && typeof instance.payloadSchema.validate === 'function'
      ? chainPromise.then(() => instance.validatePayload())
      : chainPromise;

    chainPromise = instance.querySchema && typeof instance.querySchema.validate === 'function'
      ? chainPromise.then(() => instance.validateQuery())
      : chainPromise;

    chainPromise = instance.rules
      ? chainPromise.then(() => instance.validateRules())
      : chainPromise;

    chainPromise = instance.rights
      ? chainPromise.then(() => instance.validateRights())
      : chainPromise;

    return chainPromise;
  }

  context () {
    return this.contextData;
  }

  // session () {
  //   return this.sessionData;
  // }

  validatePayload () {
    const ret = this.payloadSchema.validate(this.contextData.payload);

    if (ret.error) {
      throw new BadRequestError(ret.error);
      // throw Errors(this.errorSchema, ret);
    }
  }

  validateQuery () {
    const ret = this.querySchema.validate(this.contextData.httpQuery);

    if (ret.error) {
      throw new BadRequestError(ret.error);
      // throw Errors(this.errorSchema, ret);
    }
  }

  validateRules ([rule, ...rest] = Object.values(this.rules)) {
    const ruleResponse = rule.call(this, this.payload);

    if (!rest.length) return ruleResponse;
    if (ruleResponse && ruleResponse.then) return ruleResponse.then(() => this.validateRules(rest));

    return Promise.resolve().then(() => this.validateRules(rest));
  }

  validateRights ([rule, ...rest] = Object.values(this.rights)) {
    const ruleResponse = rule.call(this, this.payload);

    if (!rest.length) return ruleResponse;
    if (ruleResponse && ruleResponse.then) {
      return ruleResponse
        .then(() => this.validateRights(rest));
    }

    return Promise.resolve().then(() => this.validateRights(rest));
  }
}

const rulerSchema = Joi.object({
  querySchema: Joi.object().schema(),
  payloadSchema: Joi.object().schema(),
  errorSchema: Joi.object(),
  contextData: Joi.object(),
  sessionData: Joi.object(),
  payload: Joi.object(),
  httpQuery: Joi.object(),
  context: Joi.function(),
  session: Joi.function(),
  validatePayload: Joi.function(),
  validateQuery: Joi.function(),
  validateRules: Joi.function(),
  validateRights: Joi.function(),
  rules: Joi.object().pattern(Joi.string(), Joi.function()),
  rights: Joi.object().pattern(Joi.string(), Joi.function()),
}).instance(Ruler);

module.exports = Ruler;
