class NotFoundError extends Error {
  constructor (message) {
    super(message);
    this.message = message || 'Not Found';
    this.status = 404;
    // this.name = 'NotFoundErrorError';
  }
}

class BadRequestError extends Error {
  constructor (message) {
    super(message);
    this.message = message || 'Bad Request';
    this.status = 400;
  }
}

module.exports = {
  NotFoundError,
  BadRequestError,
};
