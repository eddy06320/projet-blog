const fs = require('fs');
const util = require('util');

// eslint-disable-next-line camelcase
const log_file = fs.createWriteStream(`${__dirname}/debug.log`, { flags: 'w' });
// eslint-disable-next-line camelcase
const old_log = console.log;
console.log = function a (d) {
  log_file.write(`${util.format(d)}\n`);
  old_log(d);
};
