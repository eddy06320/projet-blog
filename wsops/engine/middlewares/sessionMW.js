const session = require('express-session');
const KnexSessionStore = require('connect-session-knex')(session);
const knex = require('../knex');

const store = new KnexSessionStore({
  knex,
  clearInterval: 30 * 60 * 1000,
  tablename: 'sessions', // optional. Defaults to 'sessions'
});

module.exports = session({
  name: 'session',
  secret: 'projetX',
  cookie: {
    maxAge: 30 * 60 * 1000,
    httpOnly: true,
    // samesite: "lax",
    secure: false,
  },
  resave: false,
  saveUninitialized: false,
  store,
});
