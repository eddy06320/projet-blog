module.exports = (...roles) => ({ session }, res, next) => {
  // eslint-disable-next-line max-len
  if (session && session.user && session.user.hasRole && roles.includes(session.user.hasRole.role)) next();
  else res.sendStatus(401);
};
