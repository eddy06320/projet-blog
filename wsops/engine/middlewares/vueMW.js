const express = require('express');
const history = require('connect-history-api-fallback');

module.exports = (staticFolderName) => {
  const mw = express.Router();
  const path = `${__dirname}/${staticFolderName}`;
  console.log(path);

  mw.use(history());
  mw.use(express.static(path));

  return mw;
};
