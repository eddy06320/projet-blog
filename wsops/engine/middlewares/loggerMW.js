module.exports = () => (req, res, next) => {
  // console.log(req);
  res.log = () => console.log(`[${req.method}] ::: ${new Date().toLocaleString()} ::: ${req.originalUrl}`);
  next();
};
