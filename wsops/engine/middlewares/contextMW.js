module.exports = ({ useTrx = false } = {}) => (req, res, next) => {
  const context = {};

  context.payload = {
    ...req.body,
    ...req.params,
  };
  context.httpQuery = {
    ...req.query,
  };

  req.context = context;
  next();
};
