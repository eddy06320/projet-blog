const knex = require('./engine/knex');
// knex.raw('PRAGMA foreign_keys = ON');

Promise.resolve()
  .then(() => knex.schema
    .dropTableIfExists('article')
    .then(() => {
      console.log('article');
      return knex.schema.createTable('article', (t) => {
        t.increments('aid').primary();
        t.integer('user_uid').unsigned();
        t.foreign('user_uid').references('uid').inTable('user').onDelete('CASCADE')
          .onUpdate('CASCADE');
        t.string('title', 255);
        t.text('body');
        t.date('created_at');
      });
    }))
  .then(() => knex.schema
    .dropTableIfExists('comment')
    .then(() => {
      console.log('comment');
      return knex.schema.createTable('comment', (t) => {
        t.increments('cid').primary();
        t.integer('article_aid').unsigned();
        t.foreign('article_aid').references('aid').inTable('article').onDelete('CASCADE')
          .onUpdate('CASCADE');
        t.integer('user_uid').unsigned();
        t.foreign('user_uid').references('uid').inTable('user').onDelete('CASCADE')
          .onUpdate('CASCADE');
        t.string('title', 255);
        t.text('body');
        t.date('created_at');
      });
    }))
  .then(() => knex.schema
    .dropTableIfExists('category')
    .then(() => {
      console.log('category');
      return knex.schema.createTable('category', (t) => {
        t.increments('cid').primary();
        t.string('title', 255);
      });
    }))
  .then(() => knex.schema
    .dropTableIfExists('category_has_article')
    .then(() => {
      console.log('category_has_article');
      return knex.schema.createTable('category_has_article', (t) => {
        t.integer('category_cid').unsigned();
        t.foreign('category_cid').references('cid').inTable('category').onDelete('CASCADE')
          .onUpdate('CASCADE');
        t.integer('article_aid').unsigned();
        t.foreign('article_aid').references('aid').inTable('article').onDelete('CASCADE')
          .onUpdate('CASCADE');
      });
    }))
  .then(() => knex.schema
    .dropTableIfExists('user')
    .then(() => {
      console.log('user');
      return knex.schema.createTable('user', (t) => {
        t.increments('uid').primary();
        t.string('email', 100);
        t.string('password', 255);
      });
    }))
  .then(() => knex.schema
    .dropTableIfExists('userRole')
    .then(() => {
      console.log('userRole');
      return knex.schema.createTable('userRole', (t) => {
        t.integer('userUid').unsigned();
        t.foreign('userUid').references('uid').inTable('user').onDelete('CASCADE')
          .onUpdate('CASCADE');
        t.string('role', 100);
      });
    }))
  .then(() => knex.schema
    .dropTableIfExists('userDetails')
    .then(() => {
      console.log('userDetails');
      return knex.schema.createTable('userDetails', (t) => {
        t.integer('userUid').unsigned();
        t.foreign('userUid').references('uid').inTable('user').onDelete('CASCADE')
          .onUpdate('CASCADE');
        t.string('lname', 50);
        t.string('fname', 50);
        t.string('pseudo', 50);
      });
    }))

  .then(() => knex.destroy());
