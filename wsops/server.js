const express = require('express');
const history = require('connect-history-api-fallback');
const bodyParser = require('body-parser');

const app = express();

const sessionMW = require('./engine/middlewares/sessionMW');
const corsMW = require('./engine/middlewares/corsMW');
const loggerMW = require('./engine/middlewares/loggerMW');

const hasProperty = require('./engine/utils/hasProperty');

const UserServices = require('./src/api/user/user.services');

const routes = require('./src/api/routes');

require('./engine/utils/logger');

const port = 7777;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(sessionMW);
app.use(corsMW({}));
app.use(loggerMW());

app.use((req, _, next) => {
  if (process.env.NODE_ENV === 'test' && req.query && hasProperty(req.query, 'testSession')) {
    return UserServices.getByEmail({ payload: { email: req.query.testSession }, httpQuery: {} })
      .then((fetchedUser) => {
        delete req.query.testSession;
        req.session.user = fetchedUser;
      })
      .finally(() => next());
  }
  return next();
});

app.use(history());
app.use('/', express.static(`${__dirname}/dist`));

app.use(routes);

app.use((req, res, next) => Promise.resolve()
  .then(() => {
    if (res.headersSent) return;
    if (req.method === 'OPTIONS') {
      res
        .header('Allow', 'GET,POST,PUT,DELETE,PATCH,REPORT,OPTIONS,HEAD')
        .status(200)
        .send('GET,POST,PUT,DELETE,PATCH,REPORT,OPTIONS,HEAD');
    }
  })
  .then(next)
  .catch(next));

app.use((err, req, res, next) => {
  console.log(err.message);
  console.log(err.status);
  return res.status(err.status).send(`msg: ${err.message}, status: ${err.status}`);
});

app.start = () => {
  app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));
};

module.exports = app;
