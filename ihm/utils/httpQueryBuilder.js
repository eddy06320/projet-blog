export default (httpQuery) => Object.entries(httpQuery)
  .reduce((acc, [key, name], index) => {
    if (index > 0 && index <= (Object.entries(httpQuery).length - 1)) {
      // eslint-disable-next-line no-param-reassign
      acc += `&${key}=${name}`;
    } else {
      // eslint-disable-next-line no-param-reassign
      acc += `${key}=${name}`;
    }
    return acc;
  }, '');
