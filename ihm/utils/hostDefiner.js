import conf from '../conf';

export default {
  api: `${conf.protocol}${conf.host}/api`,
};
