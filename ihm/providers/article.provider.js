import axios from 'axios';
import httpQueryBuilder from '../utils/httpQueryBuilder';
import hostDefiner from '../utils/hostDefiner';

export default class ArticleProvider {
  static get({ payload: { aid }, httpQuery }) {
    let httpQueries;
    if (httpQuery) httpQueries = httpQueryBuilder(httpQuery);

    return axios({
      method: 'get',
      url: `${hostDefiner.api}/articles/${aid}?${httpQueries}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static report({ httpQuery }) {
    let httpQueries;
    if (httpQuery) httpQueries = httpQueryBuilder(httpQuery);

    return axios({
      method: 'report',
      url: `${hostDefiner.api}/articles?${httpQueries}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static post({ payload }) {
    return axios({
      method: 'post',
      url: `${hostDefiner.api}/articles`,
      data: payload,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static put({ payload: { aid, ...article } }) {
    return axios({
      method: 'put',
      url: `${hostDefiner.api}/articles/${aid}`,
      data: article,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static delete({ payload: { aid } }) {
    return axios({
      method: 'delete',
      url: `${hostDefiner.api}/articles/${aid}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }
}
