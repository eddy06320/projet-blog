import axios from 'axios';
import httpQueryBuilder from '../utils/httpQueryBuilder';
import hostDefiner from '../utils/hostDefiner';

export default class CommentProvider {
  static report({ httpQuery }) {
    let httpQueries;
    if (httpQuery) httpQueries = httpQueryBuilder(httpQuery);

    return axios({
      method: 'report',
      url: `${hostDefiner.api}/comments?${httpQueries}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static post({ payload }) {
    return axios({
      method: 'post',
      url: `${hostDefiner.api}/comments`,
      data: payload,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static delete({ payload: { cid } }) {
    console.log(cid);
    return axios({
      method: 'delete',
      url: `${hostDefiner.api}/comments/${cid}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }
}
