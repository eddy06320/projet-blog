import axios from 'axios';
import hostDefiner from '../utils/hostDefiner';

export default class AuthenticateProvider {
  static getSession() {
    return axios.request({
      method: 'get',
      url: `${hostDefiner.api}/authenticate`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static logIn({ payload }) {
    return axios.request({
      method: 'post',
      url: `${hostDefiner.api}/authenticate`,
      data: payload,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static destroy() {
    return axios.request({
      method: 'delete',
      url: `${hostDefiner.api}/authenticate`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }
}
