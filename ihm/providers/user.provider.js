import axios from 'axios';
import httpQueryBuilder from '../utils/httpQueryBuilder';
import hostDefiner from '../utils/hostDefiner';

export default class UserProvider {
  static get({ payload: { uid }, httpQuery }) {
    console.log(uid);
    let httpQueries;
    if (httpQuery) httpQueries = httpQueryBuilder(httpQuery);

    return axios({
      method: 'get',
      url: `${hostDefiner.api}/user/${uid}?${httpQueries}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static report({ httpQuery }) {
    let httpQueries;
    if (httpQuery) httpQueries = httpQueryBuilder(httpQuery);

    return axios({
      method: 'report',
      url: `${hostDefiner.api}/user?${httpQueries}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static post({ payload }) {
    return axios({
      method: 'post',
      url: `${hostDefiner.api}/user`,
      data: payload,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static put({ payload: { uid, ...user } }) {
    return axios({
      method: 'put',
      url: `${hostDefiner.api}/user/${uid}`,
      data: user,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static delete({ payload: { uid } }) {
    return axios({
      method: 'delete',
      url: `${hostDefiner.api}/user/${uid}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }
}
