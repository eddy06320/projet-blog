import axios from 'axios';
import hostDefiner from '../utils/hostDefiner';

export default class ContactProvider {
  static postMail({ payload }) {
    return axios({
      method: 'post',
      url: `${hostDefiner.api}/contact`,
      data: payload,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }
}
