import axios from 'axios';
import httpQueryBuilder from '../utils/httpQueryBuilder';
import hostDefiner from '../utils/hostDefiner';

export default class CategoryProvider {
  static get({ payload: { cid }, httpQuery }) {
    let httpQueries;
    if (httpQuery) httpQueries = httpQueryBuilder(httpQuery);

    return axios({
      method: 'get',
      url: `${hostDefiner.api}/categories/${cid}?${httpQueries}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static report({ httpQuery }) {
    let httpQueries;
    if (httpQuery) httpQueries = httpQueryBuilder(httpQuery);

    return axios({
      method: 'report',
      url: `${hostDefiner.api}/categories?${httpQueries}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static post({ payload }) {
    return axios({
      method: 'post',
      url: `${hostDefiner.api}/categories`,
      data: payload,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static put({ payload: { cid, ...category } }) {
    return axios({
      method: 'put',
      url: `${hostDefiner.api}/categories/${cid}`,
      data: category,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }

  static delete({ payload: { cid } }) {
    return axios({
      method: 'delete',
      url: `${hostDefiner.api}/categories/${cid}`,
    })
      .then((response) => ({ response: response.data }))
      .catch((error) => {
        console.log(error);
        return ({ error });
      });
  }
}
