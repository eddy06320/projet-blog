export default {
  methods: {
    currentDate() {
      return this.$moment().format('DD/MM/YYYY');
    },
  },
};
