const items = [
  {
    type: 'polygon', x: 1400, y: 750, sides: 6, r: 70,
  },
  {
    type: 'polygon', x: 100, y: 850, sides: 8, r: 70,
  },
  {
    type: 'circle', x: 760, y: 120, r: 70,
  },
  {
    type: 'circle', x: 850, y: 880, r: 70,
  },
  {
    type: 'circle', x: 1430, y: 90, r: 70,
  },
  {
    type: 'circle', x: 90, y: 90, r: 70,
  },
  {
    type: 'circle', x: 135, y: 285, r: 70,
  },
  {
    type: 'circle', x: 1700, y: 500, r: 70,
  },
  // {
  //   type: 'demiCircle', x: 300, y: 200, w: 200, h: 100, option:
  // { chamfer: { radius: [100, 100, 0, 0] } },
  // },
];

module.exports = items;
