const {
  Body,
  Engine,
  Render,
  Runner,
  Constraint,
  MouseConstraint,
  Mouse,
  World,
  Bodies,
  Events,
} = require('matter-js');

const items = require('./itemsPool');

export default function lauchAnim() {
  const homeElt = document.getElementById('containerAnim');

  const engine = Engine.create();
  const { world } = engine;

  // delete gravity
  engine.world.gravity.y = 0;
  engine.velocityIterations = 1;

  // create renderer
  const render = Render.create({
    element: homeElt,
    engine,
    options: {
      width: window.innerWidth,
      height: window.innerHeight,
      background: '#f5efd7',
      wireframes: false,
    },
  });

  // create runner
  const runner = Runner.create();
  Runner.run(runner, engine);

  items.forEach((item) => {
    if (item.type === 'rect' || item.type === 'demiCircle') {
    // eslint-disable-next-line no-param-reassign
      if (!item.option) item.option = {};
      const newBody = Bodies.rectangle(item.x, item.y, item.w, item.h, item.option);
      const newConstraint = Constraint.create({
        pointA: { x: item.x, y: item.y },
        bodyB: newBody,
        stiffness: 0.001,
        damping: 0.09,
        render: { visible: false },
      });
      World.add(world, [newBody, newConstraint]);
    }
    if (item.type === 'circle') {
      const newBody = Bodies.circle(item.x, item.y, item.r);
      const newConstraint = Constraint.create({
        pointA: { x: item.x, y: item.y },
        bodyB: newBody,
        stiffness: 0.001,
        damping: 0.05,
        render: { visible: false },
      });
      World.add(world, [newBody, newConstraint]);
    }
    if (item.type === 'polygon') {
      const newBody = Bodies.polygon(item.x, item.y, item.sides, item.r);
      const newConstraint = Constraint.create({
        pointA: { x: item.x, y: item.y },
        bodyB: newBody,
        stiffness: 0.001,
        damping: 0.09,
        render: { visible: false },
      });
      World.add(world, [newBody, newConstraint]);
    }
  });

  // add a circle who will follow the mouse
  const circleFollowing = Bodies.circle(400, 300, 70, {
  // stop circle when mouse not moving
    frictionAir: 0.1,
    render: {
      fillStyle: '#0000',
      strokeStyle: '#7f8c8d',
      lineWidth: 2,
    },
  });

  World.add(world, circleFollowing);

  // add mouse control
  const mouse = Mouse.create(homeElt);

  // enable conflict on google chrome
  mouse.element.removeEventListener('mousewheel', mouse.mousewheel);
  // remove drag items
  mouse.element.removeEventListener('mousedown', mouse.mousedown);

  const mouseConstraint = MouseConstraint.create(engine, {
    mouse,
  });

  World.add(world, mouseConstraint);

  function circleFollowingMouse(e) {
    Body.setPosition(circleFollowing, {
      x: e.mouse.position.x,
      y: e.mouse.position.y,
    });
  }

  // mouve the circleFollowing elemt when mouse is moving
  Events.on(mouseConstraint, 'mousemove', (e) => {
    circleFollowingMouse(e);
  });

  window.addEventListener('resize', () => {
  // rerender canvas size when window is resised
    render.canvas.width = window.innerWidth;
    render.canvas.height = window.innerHeight;
  });

  Render.run(render);
}
