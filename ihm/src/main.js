import moment from 'moment';
import axios from 'axios';
import Vue from 'vue';
import Buefy from 'buefy';
import CKEditor from 'ckeditor4-vue';
import App from './App';
import router from './router';
import store from './store';
import 'buefy/dist/buefy.css';

axios.defaults.withCredentials = true;

Vue.prototype.$moment = moment;
Vue.use(Buefy);
Vue.use(CKEditor);
Vue.config.productionTip = false;

new Vue({
  router,
  store,

  render: (h) => h(App),
}).$mount('#app');
