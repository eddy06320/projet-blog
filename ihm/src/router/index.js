import Vue from 'vue';
import VueRouter from 'vue-router';

import TheHome from '../views/TheHome';
import TheArticle from '../views/TheArticle';
import TheArticleEditor from '../views/TheArticleEditor';
import TheComment from '../views/TheComment';
import TheConnexion from '../views/TheConnexion';
import TheRegister from '../views/TheRegister';
import TheContact from '../views/TheContact';
import TheAdministrator from '../views/TheAdministrator';
import TheCategory from '../views/TheCategory';
import TheCategoryEditor from '../views/TheCategoryEditor';
import TheArticleAdministrator from '../views/TheArticleAdministrator';
import TheUserAdministrator from '../views/TheUserAdministrator';
import TheCategoryAdministrator from '../views/TheCategoryAdministrator';
import TheUserSettings from '../views/TheUserSettings';
import TheUserEditor from '../views/TheUserEditor';
import TheDecisionForm from '../views/projectForm/TheDecisionForm';
import TheMyCv from '../views/projectForm/TheMyCv';
import TheAnimatedCircles from '../views/projectAnimatedCircles/TheAnimatedCircles';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: TheHome,
  },
  {
    path: '/article/:aid',
    name: 'Article',
    component: TheArticle,
    children: [
      { path: '', component: TheComment },
    ],
  },
  {
    path: '/articleEdit/:aid',
    name: 'ArticleEditor',
    component: TheArticleEditor,
  },
  {
    path: '/login',
    name: 'Connexion',
    component: TheConnexion,
  },
  {
    path: '/signin',
    name: 'Register',
    component: TheRegister,
  },
  {
    path: '/contact',
    name: 'Contact',
    component: TheContact,
  },
  {
    path: '/administrator',
    name: 'Administrator',
    component: TheAdministrator,
    children: [
      { path: 'articleAdministrator', component: TheArticleAdministrator },
      { path: 'userAdministrator', component: TheUserAdministrator },
      { path: 'categoryAdministrator', component: TheCategoryAdministrator },
    ],
  },
  {
    path: '/category/:cid',
    name: 'Category',
    component: TheCategory,
  },
  {
    path: '/categoryEditor/:cid',
    name: 'CategoryEditor',
    component: TheCategoryEditor,
  },
  {
    path: '/userSettings',
    name: 'UserSettings',
    component: TheUserSettings,
  },

  {
    path: '/decisionForm',
    name: 'DecisionForm',
    component: TheDecisionForm,
  },
  {
    path: '/myCv',
    name: 'MyCv',
    component: TheMyCv,
  },
  {
    path: '/animatedCircles',
    name: 'AnimatedCircles',
    component: TheAnimatedCircles,
  },
  {
    path: '/userEditor/:uid',
    name: 'UserEditor',
    component: TheUserEditor,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
