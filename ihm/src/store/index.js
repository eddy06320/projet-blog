import Vue from 'vue';
import Vuex from 'vuex';

import Article from './modules/Article';
import Comment from './modules/Comment';
import Authenticate from './modules/Authenticate';
import User from './modules/User';
import Contact from './modules/Contact';
import Category from './modules/Category';
import DecisionForm from './modules/DecisionForm';
import MyCV from './modules/MyCV';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {

  },
  modules: {
    Article,
    Comment,
    Authenticate,
    User,
    Contact,
    Category,
    DecisionForm,
    MyCV,
  },
});
