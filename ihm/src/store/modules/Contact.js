import ContactProvider from '../../../providers/contact.provider';

export default {
  namespaced: true,
  state: {},
  getters: {},
  actions: {
    postMail: (_, context) => ContactProvider.postMail(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
  },
  mutations: {},
};
