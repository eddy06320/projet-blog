import UserProvider from '../../../providers/user.provider';

export default {
  namespaced: true,
  state: {
    user: {},
    userCount: {},
    userList: [],
  },
  getters: {
    getUser: (state) => state.user,
    getUserCount: (state) => state.userCount,
    getUserList: (state) => state.userList,
  },
  actions: {
    getUser: ({ commit }, context) => UserProvider.get(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_USER', response);
        return response;
      }),
    reportUserList: ({ commit }, context) => UserProvider.report(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_USER_LIST', response);
        return response;
      }),
    reportUserCount: ({ commit }) => UserProvider.report({ httpQuery: { count: true } })
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_USER_COUNT', response);
        return response;
      }),
    postUser: (_, context) => UserProvider.post(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
    putUser: (_, context) => UserProvider.put(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
    deleteUser: (_, context) => UserProvider.delete(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
    $update_user({ commit }, user) {
      commit('$UPDATE_USER', user);
    },
  },
  mutations: {
    SET_USER: (state, user) => {
      state.user = user;
    },
    SET_USER_LIST: (state, userList) => {
      state.userList = userList;
    },
    SET_USER_COUNT: (state, userCount) => {
      state.userCount = userCount;
    },
    $UPDATE_USER: (state, user) => {
      state.user = user;
    },
  },
};
