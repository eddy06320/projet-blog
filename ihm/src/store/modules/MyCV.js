import myObjectCv from './json/myCV';

export default {
  namespaced: true,
  state: {
    myCv: myObjectCv,
  },
  getters: {
    getMyCv: (state) => state.myCv,
  },
  actions: {},
  mutations: {},
};
