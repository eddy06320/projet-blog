import AuthenticateProvider from '../../../providers/authenticate.provider';

export default {
  namespaced: true,

  state: {
    userAuth: {},
  },
  getters: {
    getUserAuth: (state) => state.userAuth,
  },
  actions: {
    getSession: ({ commit }, context) => AuthenticateProvider.getSession(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_USER_AUTH', response);
        return response;
      }),
    logIn: ({ commit }, context) => AuthenticateProvider.logIn(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_USER_AUTH', response);
        return response;
      }),
    destroySession: (_, context) => AuthenticateProvider.destroy(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        return response;
      }),
  },
  mutations: {
    SET_USER_AUTH: (state, userAuth) => {
      state.userAuth = userAuth;
    },
  },
};
