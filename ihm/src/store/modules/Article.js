import ArticleProvider from '../../../providers/article.provider';

export default {
  namespaced: true,
  state: {
    article: {},
    articleCount: {},
    articleList: [],
  },
  getters: {
    getArticle: (state) => state.article,
    getArticleList: (state) => state.articleList,
    getArticleCount: (state) => state.articleCount,
  },
  actions: {
    $update_article({ commit }, article) {
      // console.log('🚀 ~ file: Article.js ~ line 17 ~ $update_article ~ article', article);
      commit('$UPDATE_ARTICLE', article);
    },
    getArticle: ({ commit }, context) => ArticleProvider.get(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_ARTICLE', response);
        return response;
      }),
    reportArticleList: ({ commit }, context) => ArticleProvider.report(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_ARTICLE_LIST', response);
        return response;
      }),
    reportArticleCount: ({ commit }) => ArticleProvider.report({ httpQuery: { count: true } })
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_ARTICLE_COUNT', response);
        return response;
      }),
    postArticle: (_, context) => ArticleProvider.post(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
    putArticle: (_, context) => ArticleProvider.put(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
    deleteArticle: (_, context) => ArticleProvider.delete(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
  },
  mutations: {
    SET_ARTICLE: (state, article) => {
      state.article = article;
    },
    $UPDATE_ARTICLE: (state, article) => {
      state.article = article;
    },
    SET_ARTICLE_LIST: (state, articleList) => {
      state.articleList = articleList;
    },
    SET_ARTICLE_COUNT: (state, articleCount) => {
      state.articleCount = articleCount;
    },
  },
};
