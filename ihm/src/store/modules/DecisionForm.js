// import decisionnerDataset from './decisionner.dataset';
import decisionnerDataset from './json/questionsPool';

export default {
  namespaced: true,

  state: {
    questionsPool: decisionnerDataset,
    repliedQuestions: [],
  },

  getters: {
    getQuestionPool: (state) => state.questionsPool,
    getRepliedQuestions: (state) => state.repliedQuestions,
    getQuestion: (state) => {
      const { repliedQuestions } = state;
      const { questionsPool } = state;

      const aswerObject = repliedQuestions[repliedQuestions.length - 1];
      if (!aswerObject) {
        return Object.values(questionsPool).find((question) => !question.condition);
      }
      return Object.values(questionsPool)
        .filter((question) => question.condition)
        .find((question) => question.condition.find((condi) => {
          if (condi.where === aswerObject.key
              && condi.what === aswerObject.answer) return question;
          return undefined;
        }));
    },
  },

  actions: {
    $setAnswerToPush({ commit }, answer) {
      commit('$SET_ANSWER_TO_PUSH', answer);
    },
    $setAnswerToPop({ commit }, answer) {
      commit('$SET_ANSWER_TO_POP', answer);
    },
  },

  mutations: {
    $SET_ANSWER_TO_PUSH(state, answer) {
      state.repliedQuestions.push(answer);
    },
    $SET_ANSWER_TO_POP(state, answer) {
      state.repliedQuestions.pop(answer);
    },
  },
};
