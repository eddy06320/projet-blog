const myCv = {
  fname: 'Eddy',
  lname: 'GROSJEAN',
  fullname: 'Eddy Grosjean',
  // profilPhoto: '../../assets/photo.jpg',
  jobTitle: 'Développeur FullStack JS Junior',
  aboutMe: 'En reconversion professionnelle, je me suis formé en autodidacte mais avec le soutien de développeurs expérimentés. J\'ambitionne  de mettre mes compétences au services d\'une entreprise à la culture innovante et ambitieuse, dans laquelle je pourrais continuer à développer mes compétences.',
  adresses: [
    { icon: 'email', value: 'grosjean.eddy@hotmail.com' },
    { icon: 'phone', value: '06.45.87.16.71' },
    { icon: 'map-marker', value: '1 escalier Bella Vista, 06320 Cap d\'Ail' },
    { icon: 'calendar', value: 'Né le 03/07/1993' },
    { icon: 'car', value: 'Permis A et B' },
  ],
  languages: [
    { value: 'Anglais', details: 'Bonnes notions', progress: 60 },
  ],
  qualities: ['Fiable et rigoureux', 'Curieux', 'Team player', 'Motivé et passionné'],
  savoirEtre: ['Développement d\'une API REST', 'Système d\'authentification', 'Utilisation d\'une API via le front', 'Gestion BDD relationnelle avec un ORM'],
  xpTechno: [
    { value: 'HTML / CSS', details: '', progress: 60 },
    { value: 'JS (Axios)', details: '', progress: 60 },
    { value: 'NodeJS (Express, Knex, Objection, Mocha)', details: '', progress: 60 },
    { value: 'VueJS (Bulma, Buefy)', details: '', progress: 60 },
    { value: 'Git / GitLab', details: '', progress: 60 },
  ],
  xpProfessional: [
    {
      duration: '2020-2021',
      title: 'Stage Développeur FullStack JS',
      company: 'ManaCorp',
      city: 'Roquebrune-Cap-Martin',
      details: '',
    },
    {
      duration: '2018-2019',
      title: 'Préparateur automobile',
      company: 'Relooking Auto',
      city: 'Labège',
      details: '',
    },
    {
      duration: '2016-2018',
      title: 'Opérateur de production',
      company: 'Sofermi',
      city: 'Saint-Orens-de-Gameville',
      details: '',
    },
    {
      duration: '2014-2016',
      title: 'Engagé volontaire à l\'Armée de Terre',
      company: '1er RCP',
      city: 'Pamier',
      details: '',
    },
  ],
  xpStudies: [
    {
      duration: '2019-2020',
      title: 'Formation Java',
      company: 'Chambre du Commerce et de l\'Industrie',
      city: 'Nice',
    },
    {
      duration: '2011-2012',
      title: 'BAC Sciences et Technologies de l\'Industrie',
      company: 'Lycée Léonard de Vinci',
      city: 'Melun',
    },
  ],
  hobbies: ['Voyage', 'Moto', 'Coder sur des projets perso'],
  reference: {
    fullname: 'Thomas LENOEL',
    company: 'ManaCorp',
    role: 'CEO',
    phone: '+33 6 98 97 31 41',
    mail: 'hello@manacorp.eu',
  },
};

module.exports = myCv;
