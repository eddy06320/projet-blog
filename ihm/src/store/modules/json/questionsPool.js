const questionsPool = {
  developmentType: {
    key: 'developmentType',
    label: 'Vous recherchez un développeur :',
    solution: {
      response: ['front', 'back', 'fullStack'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'wichTechno', what: 'JS' }],
  },
  whichTechno: {
    key: 'wichTechno',
    label: 'Quelles sont les technologies que vous utilisez ?',
    solution: {
      response: ['JS', 'PHP', 'Java', 'Python', 'Autre'],
      responseEffect: (option) => {
        let addClass = null;
        if (option === 'PHP') {
          addClass = 'phpAnswerAnimation';
        }
        if (option === 'Java') {
          addClass = 'javaAnswerAnimation';
        }
        if (option === 'Python') {
          addClass = 'pythonAnswerAnimation';
        }
        if (option === 'Autre') {
          addClass = 'otherAnimation';
        }
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'mustHaveFirstXp', what: 'non' }],
  },
  frontFrameworkType: {
    key: 'frontFrameworkType',
    label: 'Quel framework front utilisez vous ?',
    solution: {
      response: ['Vue', 'React', 'Angular'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'developmentType', what: 'front' }, { where: 'developmentType', what: 'fullStack' }],
  },
  whichTechLvl: {
    key: 'whichTechLvl',
    label: 'Quel est le niveau attendu ?', // Si c'est > junior on sort
    solution: {
      response: ['junior', 'intermediaire', 'senior'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'mustBeGraduate', what: 'non' }, { where: 'mayPickASelfEducated', what: 'oui' }],
  },
  areUReady: {
    key: 'areUReady',
    label: 'Êtes vous prêt à commencer ?', // Si c'est non on sort
    solution: {
      response: ['oui', 'non'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
  },
  areULookingFor: {
    key: 'areULookingFor',
    label: 'Êtes vous à la recherche d\'un dévoloppeur ?', // Si c'est non on sort
    solution: {
      response: ['oui', 'non'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'areUReady', what: 'oui' }],
  },
  mustBeGraduate: {
    key: 'mustBeGraduate',
    label: 'Êtes vous à la recherche d\'une personne diplômé obligatoirement ?',
    solution: {
      response: ['oui', 'non'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'areULookingFor', what: 'oui' }],
  },
  whichStudiesLvl: {
    key: 'whichStudiesLvl',
    label: 'Quel est le niveau d\'études attendu ?', // Si c'est > Bac+2 on sort
    solution: {
      response: ['<= BAC+2', '> Bac+2'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'mustBeGraduate', what: 'oui' }],
  },
  mayPickASelfEducated: {
    key: 'mayPickASelfEducated',
    label: 'Êtes vous prêt à prendre un autodidacte très motivé, même sans diplôme ?', // Si c'est < Bac+2 on sort
    solution: {
      response: ['oui', 'non'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'whichStudiesLvl', what: '<= BAC+2' }],
  },
  mustHaveFirstXp: {
    key: 'mustHaveFirstXp',
    label: 'Exigez vous une première expérience professionnelle ?',
    solution: {
      response: ['oui', 'non'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'whichTechLvl', what: 'junior' }],
  },
  internalTraining: {
    key: 'internalTraining',
    label: 'Proposez vous des formations en internes ?',
    solution: {
      response: ['oui', 'non'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'frontFrameworkType', what: 'React' }, { where: 'frontFrameworkType', what: 'Angular' }],
  },
  describeYourCompany: {
    key: 'describeYourCompany',
    label: 'Quel adjectif décrirait le mieux la mentalité de votre entreprise ?',
    solution: {
      response: ['Ambitieuse', 'Rigoureuse', 'Créative'],
      responseEffect: () => {
        const addClass = null;
        return addClass;
      },
    },
    answer: '',
    condition: [{ where: 'internalTraining', what: 'oui' }, { where: 'frontFrameworkType', what: 'Vue' }, { where: 'developmentType', what: 'back' }],
  },
};

module.exports = questionsPool;
