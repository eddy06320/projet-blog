import CommentProvider from '../../../providers/comment.provider';

export default {
  namespaced: true,
  state: {
    commentCount: {},
    commentList: [],
  },
  getters: {
    getCommentCount: (state) => state.commentCount,
    getCommentList: (state) => state.commentList,
  },
  actions: {
    reportCommentCount: ({ commit }) => CommentProvider.report({ httpQuery: { count: true } })
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_COMMENT_COUNT', response);
        return response;
      }),
    reportCommentList: ({ commit }, context) => CommentProvider.report(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_COMMENT_LIST', response);
        return response;
      }),
    postComment: (_, context) => CommentProvider.post(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
    deleteComment: (_, context) => CommentProvider.delete(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
  },
  mutations: {
    SET_COMMENT_COUNT: (state, commentCount) => {
      state.commentCount = commentCount;
    },
    SET_COMMENT_LIST: (state, commentList) => {
      state.commentList = commentList;
    },
  },
};
