import CategoryProvider from '../../../providers/category.provider';

export default {
  namespaced: true,
  state: {
    category: {},
    categoryWithRelatedArticles: {},
    categoryList: [],
  },
  getters: {
    getCategory: (state) => state.category,
    getCategoryWithRelatedArticles: (state) => state.categoryWithRelatedArticles,
    getCategoryList: (state) => state.categoryList,
  },
  actions: {
    getCategory: ({ commit }, context) => CategoryProvider.get(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_CATEGORY', response);
        return response;
      }),
    getCategoryWithRelatedArticles: ({ commit }, context) => CategoryProvider.get(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_CATEGORY_WITH_RELATED_ARTICLES', response);
        return response;
      }),
    reportCategoryList: ({ commit }, context) => CategoryProvider.report(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }
        commit('SET_CATEGORY_LIST', response);
        return response;
      }),
    postCategory: (_, context) => CategoryProvider.post(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
    putCategory: (_, context) => CategoryProvider.put(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
    deleteCategory: (_, context) => CategoryProvider.delete(context)
      .then(({ response, error }) => {
        if (error) { console.log(error); return response; }

        return response;
      }),
    $update_category({ commit }, category) {
      commit('$UPDATE_CATEGORY', category);
    },
  },
  mutations: {
    SET_CATEGORY: (state, category) => {
      state.category = category;
    },
    SET_CATEGORY_WITH_RELATED_ARTICLES: (state, categoryWithRelatedArticles) => {
      state.categoryWithRelatedArticles = categoryWithRelatedArticles;
    },
    SET_CATEGORY_LIST: (state, categoryList) => {
      state.categoryList = categoryList;
    },
    $UPDATE_CATEGORY: (state, category) => {
      state.category = category;
    },
  },
};
