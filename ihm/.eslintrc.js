module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/recommended',
    '@vue/airbnb',
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'import/extensions': ['error', 'ignorePackages', {
      ts: 'never',
      tsx: 'never',
      js: 'never',
      jsx: 'never',
      vue: 'never',
      mjs: 'never',
    }],
    'max-len': ['error', { code: 100, ignoreStrings: true, comments: 100 }],
    'vue/custom-event-name-casing': 'off',
  },

  settings: {
    'import/resolver': {
      node: {
        paths: ['src'],
      },
      webpack: {
        config: require.resolve('@vue/cli-service/webpack.config.js'),
      },
    },
  },

  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        mocha: true,
      },
    },
  ],
};
