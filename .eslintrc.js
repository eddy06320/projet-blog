module.exports = {
  root: true,
  env: {
    node: true,
    mocha: true,
  },
  extends: [
    'airbnb-standard',
    'plugin:mocha/recommended',
  ],
  globals: {
    wsops: 'readonly',
    expect: 'readonly',
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2020,
    ecmaFeatures: { jsx: false },
    babelOptions: {
      // configFile: join(__dirname, './.babelrc.js'),
    },
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-param-reassign': ['error', { props: false }],
    'max-classes-per-file': 'off',
    // 'function-paren-newline': ['error', 'multiline-arguments'],
    'global-require': 'off',
    'mocha/no-skipped-tests': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'mocha/no-exclusive-tests': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'mocha/no-mocha-arrows': 'off',
    'mocha/no-setup-in-describe': 'off',
    'mocha/no-hooks-for-single-case': 'off',
    'mocha/no-top-level-hooks': 'off',
    'spaced-comment': ['error', 'always', { line: { exceptions: ['*', '/'] }, block: { exceptions: ['/'] } }],
    'quote-props': ['error', 'consistent-as-needed'],

    'indent': ['error', 2, { SwitchCase: 1 }],
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'never',
      exports: 'never',
      functions: 'never',
    }],

  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        mocha: true,
      },
    },
  ],
};
